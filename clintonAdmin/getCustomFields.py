import requests
from requests.auth import HTTPBasicAuth
import json
import yaml
import os



# check whether the file exists
if os.path.exists("JIRAcustomFields.yaml"):
    # delete the file
    os.remove("JIRAcustomFields.yaml")

# check whether the file exists
if os.path.exists("JIRACMcustomFields.yaml"):
    # delete the file
    os.remove("JIRACMcustomFields.yaml")


exampleCM = "CM-90850" #give an example CM that exists this one is from cloud JIRA

jiraPRDCloudBase = "https://skytouchtechnology.atlassian.net/rest/api/3"
jiraSandboxBase ="https://skytouchtechnology-sandbox-114.atlassian.net/rest/api/3"

jiraCloudBase = jiraPRDCloudBase

basic = HTTPBasicAuth(user, apiKey)


jiraResponse = requests.get(jiraCloudBase+"/field",auth=basic)

jiraFields = json.loads(jiraResponse.text)

jiracustomFields = {}
jiraFieldsToGet = ["Organizational Domain","Epic Link","Affects Build Number","Platform",
               "Found in","Exists In","Severity","QA RCA","QA RCA Analysis","Fix Build Number",
               "Production Deploy Date","QA Deploy Date","Business Justification","Cadence","Cut Release Date"]

for fieldNeeded in jiraFieldsToGet:
  jiracustomFields[fieldNeeded] = "Unable to Find"
  for field in jiraFields:
    if field["custom"]:
        name = field["name"]
        if name == fieldNeeded: 
          jiracustomFields[fieldNeeded] = field["id"]


jiraCMResponse = requests.get(jiraCloudBase+"/issue/"+exampleCM+"?expand=names",auth=basic)


jiraCMFields = json.loads(jiraCMResponse.text)

jiraCMcustomFields = {}
jiraCMFieldsToGet = ["Change Type","Environment","Platform","Release Version.Build",
               "Release Notes","Business Justification (migrated)","Change Risk","Change Reason","Impact","Change start date",
               "Change completion date","QA Deploy Date","Component/s","Request participants","Change start date actual",
               "Change completion date actual","Planned Outage Start Time","Planned Outage End Time","Cut Time","Int Deploy Time","Int Test Time","QA Deploy Time", "QA Test Time"]

for fieldNeeded in jiraCMFieldsToGet:
  jiraCMcustomFields[fieldNeeded] = "Unable to Find"
  for key,name in jiraCMFields["names"].items():
    if "customfield_" in key:
        if name.lower() == fieldNeeded.lower(): 
          jiraCMcustomFields[fieldNeeded] = key


file1=open("JIRAcustomFields.yaml","w")
yaml.dump(jiracustomFields,file1)
file1.close()
print("JIRAcustomFields.yaml file saved.")

file2=open("JIRACMcustomFields.yaml","w")
yaml.dump(jiraCMcustomFields,file2)
file2.close()
print("JIRACMcustomFields.yaml file saved.")
