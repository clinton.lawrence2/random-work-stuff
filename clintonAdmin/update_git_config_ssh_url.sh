#!/bin/bash
#update_git_config_ssh_url.sh

# Save the current directory as the script's run directory
script_run_directory=$(pwd)

# Prompt for the base repository path where multiple Git repositories are located
echo "Enter the base path containing your Git repositories:"
read -r base_repo_path

# Correctly terminate the find command's -exec option
find "$base_repo_path" -type d -exec bash -c 'cd "{}" && pwd && if [ -d ".git" ]; then python3 - <<EOF
import csv
import subprocess
import os

# Script run directory for CSV and log file locations
script_run_directory = "'$script_run_directory'"

# Define the CSV file path
csv_file_path = os.path.join(script_run_directory, "future_urls.csv")

# Define the log file path (will be created in the script run directory)
log_file_path = os.path.join(script_run_directory, "updatedGitUrls.log")

# Function to get the current Git URL
def get_current_git_url():
    try:
        current_url = subprocess.check_output(["git", "remote", "get-url", "origin"], stderr=subprocess.STDOUT).decode("utf-8").strip()
        return current_url
    except subprocess.CalledProcessError as e:
        return None

# Function to update Git URL
def update_git_url(old_url, new_url):
    try:
        subprocess.check_call(["git", "remote", "set-url", "origin", new_url])
        return True
    except subprocess.CalledProcessError:
        return False

# Main script
def main():
    with open(csv_file_path, mode="r") as csv_file, open(log_file_path, mode="a") as log_file:
        csv_reader = csv.DictReader(csv_file)
        current_url = get_current_git_url()
        if not current_url:
            return
        for row in csv_reader:
            old_url = row["current_git_ssh"]
            new_url = row["future_git_ssh"]
            if current_url == old_url:
                # Update the Git URL
                if update_git_url(old_url, new_url):
                    log_file.write(f"{os.getcwd()}: {old_url} -> {new_url}\n")
                else:
                    log_file.write(f"Failed to update {os.getcwd()}: {old_url} -> {new_url}\n")

if __name__ == "__main__":
    main()
EOF
else echo "Not a Git repository: {}"; fi' \;