(function() {
  'use strict';

  function a(a) {
      return a in e ? e[a] : e.en
  }

  function b(a) {
      const b = [];
      let c = 0;
      return a.forEach(a => {
          const c = a.innerHTML,
              d = c.split(",");
          b.push(...d)
      }), b.filter(a => a).forEach(a => {
          a = a.trim(), a.endsWith("s") && (a = a.substring(0, a.length - 1));
          const b = a.split(" "),
              d = parseInt(b[0]),
              e = h[b[1]];
          c += d * e
      }), c
  }

  function c() {
      const b = {
              navigator: ".navigator-container",
              details: ".details-layout",
              estimateColumn: ".timeestimate"
          },
          c = a(document.documentElement.lang),
          d = null !== document.querySelector(b.navigator);
      if (!d) throw new Error(c.goToNavigator());
      const e = null !== document.querySelector(b.details);
      if (e) throw new Error(c.changeViewTypeToList());
      const f = document.querySelectorAll(b.estimateColumn);
      if (1 > f.length) throw new Error(c.enableRemainingEstimateColumn());
      return f
  }

  function d() {
      const d = a(document.documentElement.lang),
          e = c(),
          f = b(e),
          i = (f / (h.hour * g)) * 8,
          j = i.toFixed(1);
      alert(d.estimateOutput(j))
  }
  const e = {
          en: {
              pleaseEnter: () => "Please enter the cost per hour in \u20AC:",
              costResult: a => `Estimatet cost for displayed issues: ${a} €.`,
              notANumber: () => "This is not a number. Please try again.",
              estimateOutput: a => `Estimated remaining effort for the displayed issues: ${a} Hours.`,
              goToNavigator: () => "Error: Please go to the Jira issue navigator.",
              changeViewTypeToList: () => "Error: Please change view type to list.",
              enableRemainingEstimateColumn: () => "Error: Please enable the column '\u03A3 Remaining Estimate.'"
          }
      },
      f = 60,
      g = 8,
      h = {
          week: 5 * g * f,
          day: g * f,
          hour: f,
          minute: 1
      };
  try {
      d()
  } catch (a) {
      alert(a.message)
  }
})();