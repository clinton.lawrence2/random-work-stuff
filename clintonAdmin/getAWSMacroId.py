import boto3
import argparse_prompt
import re
import logging
import subprocess

logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.INFO)
logging.getLogger('boto3').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)



INT_ENVS = [
  "intci",
  "intchi",
  "int01",
  "int02",
  "int03",
  "int04",
  "int05",
  "int06",
  "int07",
  "int08",
  "int09",
  "int10",
  "int11",
  "int12",
  "int13",
  "int14",
  "int15",
  "int16",
  "int17",
  "int18",
  "int19",
  "int20"
]

ST_QAT_ENVS = ["crt01", "qahose01"]
CHI_LT_ENVS = ["lt", "lt2"]
CHI_QAT_ENVS = [
  "qat1",
  "rit",
  "sit",
  "lt",
  "lt2"
]
ST_PRD_ENVS = ["prd01"]
CHI_PRD_ENVS = ["prd2"]
SPS_ENVS = ["sps"]
CHISPS_ENVS = ["chisps"]

AWS_MACRO_ENV = {
  "INT"   : INT_ENVS,
  "QAT"   : ST_QAT_ENVS,
  "CHILT": CHI_LT_ENVS,
  "CHIQAT": CHI_QAT_ENVS,
  "PRD" : ST_PRD_ENVS,
  "CHIPRD": CHI_PRD_ENVS,
  "SPS": SPS_ENVS,
  "CHISPS": CHISPS_ENVS
}

def getMacroId(appEnvId="") -> str:
  macroId = next((key for key, value in AWS_MACRO_ENV.items() if appEnvId in value), "")
  return macroId.lower()
  

if __name__ == "__main__":

  #setup parser so we can take command line arguments
  parser = argparse_prompt.PromptParser()
  parser.add_argument('--appEnvId',        help="AppenvId to ",type=str)
  args = parser.parse_args()
  appEnvId = args.appEnvId
  macroId = getMacroId(appEnvId=appEnvId)
  print(macroId)
  