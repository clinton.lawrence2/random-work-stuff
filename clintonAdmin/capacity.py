from datetime import datetime
from datetime import timedelta 
import math

hoursInDay = 8

startDate = input("Sprint Start Date(MM-DD-YYYY): ") 
daysInSprint = input("Sprint Length(days include weekend,default 13): ")

if daysInSprint == "":
  daysInSprint = 13

startDateObj = datetime.strptime(startDate, '%m-%d-%Y')
endDateObj =  startDateObj + timedelta(days=int(daysInSprint))



daysCount = (endDateObj - startDateObj).days

weekendDays = ["Saturday","Sunday"]


daysData = {}
totalNonProjTime = 0
for i in range(0,daysCount):
  tmpDate = startDateObj + timedelta(days=i)
  tmpDay = tmpDate.strftime ("%A") 
  tmpDateStr = tmpDate.strftime('%Y-%m-%d')
  if tmpDay in weekendDays:
    continue
  nonProjTime = float(input(tmpDay + ", " + tmpDateStr+" Non Proj Time: "))
  totalNonProjTime += nonProjTime

print( "Total Non Proj For this Sprint: "+str(math.ceil(totalNonProjTime)))