import boto3
import argparse_prompt
import re
import logging

logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.INFO)
logging.getLogger('boto3').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)

elbClient = boto3.client('elb')
elbv2Client = boto3.client('elbv2')
r53Client = boto3.client('route53')

def getFromALB(albName="",dnsName="") -> str:
  instanceId = ""
  albArn = elbv2Client.describe_load_balancers(Names=[albName])["LoadBalancers"][0]["LoadBalancerArn"]
  listeners = elbv2Client.describe_listeners(
    LoadBalancerArn=albArn)["Listeners"]
  for listener in listeners:
    if listener["Protocol"] == "HTTPS":
      listenerArn = listener["ListenerArn"]
      rules = elbv2Client.describe_rules(ListenerArn=listenerArn)["Rules"]
      for rule in rules:
        for condition in rule["Conditions"]:
          if condition["Field"] == "host-header" and dnsName in condition["Values"]:
            tgArn = rule["Actions"][0]["TargetGroupArn"]
            logging.info("TargetGroup: "+ tgArn)
            instanceId = elbv2Client.describe_target_health(
              TargetGroupArn=tgArn)["TargetHealthDescriptions"][0]['Target']['Id']

  return instanceId
        

def getFromELB(elbName="") -> str:
  logging.info("ELB Name:"+elbName)
  instanceId = elbClient.describe_load_balancers(
    LoadBalancerNames=[elbName])["LoadBalancerDescriptions"][0]["Instances"][0]["InstanceId"]
  return instanceId

def getInternalDNS(dnsName=""):
  zoneDns = dnsName.split(".",1)[1]
  logging.info("ZoneDNS: "+zoneDns)

  hostedZones = r53Client.list_hosted_zones_by_name(
    DNSName=zoneDns
)

  zoneId = ""

  for zone in hostedZones["HostedZones"]:  
    if zoneDns+"." == zone["Name"]:
      zoneId = zone["Id"].split("/hostedzone/")[1]
      logging.info("hosted zone id: "+zoneId)

  internalDNS = r53Client.list_resource_record_sets(
    HostedZoneId=zoneId,
    StartRecordName=dnsName,
    MaxItems="1")["ResourceRecordSets"][0]["ResourceRecords"][0]["Value"]

  logging.info("internalDNS: "+ internalDNS)
  return internalDNS

def getNameFromDNS(dns="") -> str:
  updatedDns = dns.replace("internal-", "")
  match = re.search(r"-\d+", updatedDns)
  items = []
  if match:
      updatedDns = updatedDns[:match.start()]
  
  logging.info("DNS SPLIT: "+str(updatedDns))
  return updatedDns


if __name__ == "__main__":

  #setup parser so we can take command line arguments
  parser = argparse_prompt.PromptParser()
  parser.add_argument('--dns',        help="DNS record to get values from",type=str)
  args = parser.parse_args()
  dnsName = args.dns

  internalDNS = getInternalDNS(dnsName=dnsName)
  logging.info("InternalDNS: "+internalDNS)

  instanceId = ""
  internalDNS = internalDNS.split(".",1)[0]
  lbName = getNameFromDNS(internalDNS).strip()
  logging.info("LB Name: "+lbName)

  if "-alb-" in internalDNS:
    
    instanceId = getFromALB(lbName,dnsName)
  else:
    instanceId = getFromELB(lbName)

  logging.info("InstanceId: "+instanceId)
  print(instanceId)
