#!/bin/zsh -x

echo ENVIRONMENT=CHIPRD >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_REPO_URL=ssh://git@scm.skytouch.io/deploy/deployment.git >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_BRANCH=master >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_VARS_REPO_URL=ssh://git@scm.skytouch.io/av/st-vars-prd.git >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_VARS_BRANCH=master >> /tmp/bootstrap.config
echo APP_ENVIRONMENT_ID=prd01 >> /tmp/bootstrap.config
echo APP_NAME=edi-sender >> /tmp/bootstrap.config
echo APP_VERSION= >> /tmp/bootstrap.config
echo CLOUD_CONFIG_REPO_BRANCH=master  >> /tmp/bootstrap.config
export GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
mkdir /tmp/st-infra/
aws s3 cp s3://st-vars-int/id_rsa /root/.ssh/id_rsa
chmod 400 /root/.ssh/id_rsa
git clone -b master ssh://git@scm.skytouch.io/infra/st-infra.git /tmp/st-infra/
cp /tmp/st-infra/bootstrap-scripts/bootstrap.sh /tmp/bootstrap.sh
rm -rf /tmp/st-infra/*
stdbuf -i0 -o0 -e0 zsh /tmp/bootstrap.sh