export FreeMem=$(free -m | grep buffers/cache | awk '{ print $4 }')
FreeMem=$(free -m | grep Mem | awk '{ print $7 }')
TotalMem=$(free -m | grep Mem | awk '{ print $2 }')
FreePer=$(awk -v total="$TotalMem" -v free="$FreeMem" 'BEGIN { printf("%-10f\n", (free / total) * 100) }' | cut -d. -f1)
UsedPer=$((100-$FreePer))
echo "system memory usage: $UsedPer%"