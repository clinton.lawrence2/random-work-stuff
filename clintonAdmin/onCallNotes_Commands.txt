#Find Large Files
sudo du -aBm <dir> 2>/dev/null | sort -nr | head -n 10

#zip
gzip -v <filename>

#determine xen vs nitro type
aws ec2 describe-instance-types --instance-type <instance_type> --query "InstanceTypes[].Hypervisor" --profile int

#resize partition to filesystem nitro

Confirm if partition size mismatch or logical volume mistmach
  sudo lsblk (take node of partition or lvm name)

if needed grow partition
  sudo growpart /dev/NAMEOFPARTIION NUMBER partition
  sudo resize2fs /dev/xvda1
  
if needed fix logical volume size mismatch
  sudo pvresize $(pvdisplay -s | awk -F "\"" '{ print $2 }') && sudo lvextend -l +100%FREE /dev/mapper/vgopt-lvopt && sudo resize2fs /dev/vgopt/lvopt
  
  
# check logins
last