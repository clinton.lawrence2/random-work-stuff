#!/bin/zsh -x

echo ENVIRONMENT=INT >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_REPO_URL=ssh://git@scm.skytouch.io/deploy/deployment.git >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_BRANCH=CP-668-investigate-removing-internet-connectivity-requirements-from-environments >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_VARS_REPO_URL=ssh://git@scm.skytouch.io/av/st-vars.git >> /tmp/bootstrap.config
echo GIT_DEPLOYMENT_VARS_BRANCH=master >> /tmp/bootstrap.config
echo APP_ENVIRONMENT_ID=int >> /tmp/bootstrap.config
echo APP_NAME=jaspersoft >> /tmp/bootstrap.config
echo APP_VERSION= >> /tmp/bootstrap.config
echo CLOUD_CONFIG_REPO_BRANCH=master  >> /tmp/bootstrap.config
export GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
sudo mkdir /tmp/st-infra/
sudo aws s3 cp s3://st-vars-int/id_rsa /root/.ssh/id_rsa       
sudo chmod 400 /root/.ssh/id_rsa
sudo git clone -b master ssh://git@scm.skytouch.io/infra/st-infra.git /tmp/st-infra/
sudo cp /tmp/st-infra/bootstrap-scripts/bootstrap.sh /tmp/bootstrap.sh
sudo rm -rf /tmp/st-infra/*
sudo stdbuf -i0 -o0 -e0 zsh /tmp/bootstrap.sh >> /tmp/bootstrap.log


apt-mirror: 10.64.26.182
apt-mirror: 10.64.1.191