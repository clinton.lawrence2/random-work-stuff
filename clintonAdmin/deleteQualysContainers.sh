count=0
count=$(curl -v -X GET --header 'Accept: application/json' --header 'Authorization: Basic c2t5dHUzamo6bHMkNkhnU1ZkcQ==' 'https://qualysapi.qg3.apps.qualys.com/csapi/v1.1/containers?filter=state%3ASTOPPED&pageNo=1&pageSize=1&sort=created%3Adesc' | jq '.count')

echo "\n COUNT: $count"

pageSize=1000

while [ $count -gt 0 ]; do
    curl -X GET --header 'Accept: application/json' --header 'Authorization: Basic c2t5dHUzamo6bHMkNkhnU1ZkcQ==' 'https://qualysapi.qg3.apps.qualys.com/csapi/v1.1/containers?filter=state%3ASTOPPED&pageNo=1&pageSize='+$pageSize+'&sort=created%3Adesc' -o containers.json

    cat containers.json | jq '{containerIds : [ .data[].uuid ] }' > containerId.json

    cat containerId.json

    curl -X DELETE --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: Basic c2t5dHUzamo6bHMkNkhnU1ZkcQ==' -d @containerId.json 'https://qualysapi.qg3.apps.qualys.com/csapi/v1.1/containers'

    (( count = $count - $pageSize ))

    echo "\nCOUNT: $count"

done