      #!/bin/bash
      # Summary: Tries and deletes files we don't need to free up space
      # Author: Tyler Monahan
      # Date: Sep 26 2023
      
      # Delete compressed files in /var/log and opt logs
      find /var/log -type f -name "*.gz" -exec rm -f {} \;
      find /opt/*/logs -type f -name "*.zip" -exec rm -f {} \;
      find /opt/*/logs -type f -name "*.gz" -exec rm -f {} \;
      
      # truncate logs in opt logs
      find /opt/*/logs -type f -name "*.log" -exec truncate -s 0 {} \;
      
      # Delete tmp and root home directory.
      rm -r /tmp/*
      rm -r /root/*

      # Fail if disk space is still greater then 90%
      set -e
      df -Ph / | awk 'int($5) > 90 {print $0; rc=1}; END {exit rc}'
      df -Ph /opt | awk 'int($5) > 90 {print $0; rc=1}; END {exit rc}'