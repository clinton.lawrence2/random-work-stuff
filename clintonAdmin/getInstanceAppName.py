import boto3
import argparse_prompt
import logging
import subprocess
import getAWSMacroId
import time

logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.INFO)
logging.getLogger('boto3').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)

ec2Client = boto3.client('ec2')


def getFromAppName(appName="",appEnvId="") -> list:
  
  response = ec2Client.describe_instances(
       Filters=[
        {
            'Name': 'tag:Name',
            'Values': [appName]
        },
        {
            'Name': 'instance-state-name',
            'Values': ["running"]
        },
         {
            'Name': 'tag:app_environment_id',
            'Values': [appEnvId]
        },
    ]
    )
  logging.debug("Raw Instance data found: {rawResponse}".format(rawResponse=response))
  instanceIds = []
  for reservation in response['Reservations']:
    for instance in reservation['Instances']:
        instanceIds.append(instance['InstanceId'])
  
  return instanceIds
        



if __name__ == "__main__":

  #setup parser so we can take command line arguments
  parser = argparse_prompt.PromptParser()
  parser.add_argument('--appName',        help="DNS record to get values from",type=str)
  parser.add_argument('--appEnvId',        help="AppenvId to ",type=str)
  args = parser.parse_args()
  appName = args.appName
  appEnvId = args.appEnvId
  
  logging.info("Getting EC2 Instances for {appname} from {awsenv}".format(appname=appName,awsenv=appEnvId))
  instances = getFromAppName(appName=appName,appEnvId=appEnvId)
  logging.info("Found {numberEc2} instances of {appName}".format(numberEc2=len(instances),appName=appName))
  
  newwindow = f'osascript -e \'tell application "iTerm" to tell current window to create window with default profile\''
  subprocess.run(newwindow, shell=True, check=True)
  time.sleep(3)

  macroId = getAWSMacroId.getMacroId(appEnvId=appEnvId)

  for i, instanceID in enumerate(instances):
    logging.info("Conneting to EC2 Instance for {instanceID}".format(instanceID=instanceID))
    # Define the iTerm command to run the alias
    sshCommand = 'sshFromId {instanceID} {awsenv}'.format(instanceID=instanceID,awsenv=macroId)
 
    # Run the iTerm command
    try:
        runssh=f'osascript  -e \'tell application "iTerm" to tell current session of current window to write text "{sshCommand}"\''
        subprocess.run(runssh, shell=True, check=True)
        if i + 1 < len(instances):
          newsession = f'osascript -e \'tell application "iTerm" to tell current session of current window to split horizontally with default profile\''
          subprocess.run(newsession, shell=True, check=True)
          time.sleep(20)
    except subprocess.CalledProcessError as e:
        print(f"Error running iTerm command: {e}")
