#Get AWS Stack Count per Account and compare against our quota
import boto3
import logging

logging.getLogger('boto3').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)
logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.INFO)


#aws cloudformation describe-stacks --query "Stacks[].[StackName]"
def getStacks(client) -> list:
  nextToken = None
  stacks = []
  keepSearching = False
  response = client.describe_stacks()
  for stack in response['Stacks']:
      stacks.append(stack['StackName'])
      #get keys from map
      keepSearching = "NextToken" in list(response.keys())
      if keepSearching:
        nextToken = response['NextToken']
      else:
        logging.info("Done Searching Stacks")

  while keepSearching:
    response = client.describe_stacks(NextToken=nextToken)
    for stack in response['Stacks']:
        stacks.append(stack['StackName'])
        keepSearching = "NextToken" in list(response.keys())
        if keepSearching:
          nextToken = response['NextToken']
        else:
          logging.info("Done Searching Stacks")
          break
  return stacks 

def getStackQuotaValue(client,quotaName):
  response = client.get_service_quota(
    ServiceCode='cloudformation',
    QuotaCode=quotaName
  )
  return response['Quota']['Value']

def main():
    cnfClient = boto3.client('cloudformation')
    quotaClient = boto3.client('service-quotas') 
    stacks = getStacks(cnfClient)
    quotaValue = getStackQuotaValue(quotaClient, "L-0485CB21")
    count = len(stacks)
    logging.info("Stack Count:"+str(count))
    logging.info("Quota Value:"+str(quotaValue))

    percentageOfQuota = round((count/quotaValue)*100,2)
    logging.info("Percentage of Quota:"+str(percentageOfQuota))

if __name__ == "__main__":
    main()