dbName=$1
forbiddenValues="intci qat1 crt01 rit sit lt lt2 lt3 qahose01"

# Extracting the environment
dbEnv="${dbName%%\-*}"


if [[ $dbEnv =~ int[0-9]{2}$ ]]; then
    echo "DB Latest Allowed to be Ran in this environment"
    exit 0
elif [[ $forbiddenValues =~ (^| )$dbEnv($| ) ]]; then
    echo "DB Latest NOT allowed to be Ran one of these environemnts INTCI, QAT1, CRT01, RIT, SIT, LT, LT2, LT3, QAHOES01"
    exit 1
else
    echo "Unknown Environment $dbEnv"
    echo "Only Accepted Values are int## environment where ## is two digit reference to environment ie int03"
    exit 1
fi
