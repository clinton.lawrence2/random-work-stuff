#for all directories in current directory loop through and run a command
for dir in */

numLoops=$1
#run a loop for based on a number input
for i in $(seq 1 $numLoops)
do
  #when i < 10 do something else
  if [ $i -lt 10 ]; then
    echo 0$i
  else
	  echo $i
  fi
done
