# Add users to docker hub organization

import requests
import argparse_prompt
baseUrl = "https://hub.docker.com/v2"

def getToken(username,password):
    url = baseUrl + "/users/login/"
    headers = {
        "Content-Type": "application/json",
    }
    data = {
        "username": username,
        "password": password
    }
    response = requests.post(url, headers=headers, json=data)
    if response.status_code != 200:
        print("Error logging in")
        print(response.text)
        return None
    else:
        print("Logged in successfully")
        print(response.text)
        return response.json()["token"]

def add_users(users, organization,token):
  
  addUserUrl = baseUrl + "/invites/bulk"
  headers = { "Authorization": "Bearer " + token }
  emailList = []
  for user in users: 
     emailList.append(user)

  print("Email List:"+str(emailList))
  data = {
    "invitees": emailList,
    "org": organization,
    "team":"skytouch",
    "dry_run":False,
    "role":"member"
  }

  response = requests.post(addUserUrl, headers=headers, json=data)
  print(response.text)
  if response.status_code == 202:
     for invitee in response.json()["invitees"]:
      print(invitee["status"])
      if("invited" == invitee["status"]):
          print(invitee["invitee"] + " added successfully")
      else:
         print(invitee["invitee"] + " NOT added successfully "+ invitee["status"])
  else:
    print("Error adding users")
    print(response.status_code)
    print(data)
    print(response.text)

if __name__ == "__main__":
  #cli args prompt object to capture dockerhub email password and users
  parser = argparse_prompt.PromptParser(description='Add users to docker hub organization')
  parser.add_argument('--users', type=str ,help='Users(email) to add to docker hub organization(comma seperated)')
  parser.add_argument('--organization', help='Organization to add users to',default="choicehotels")
  parser.add_argument('--loginUser', help='Dockerhub username', default="XXXXXXXXXXX")
  parser.add_argument('--loginPassword', help='Dockerhub password', default="XXXXXXXXXXX")

  args = parser.parse_args()
  print(args)
  users = args.users.split(',')
  organization = args.organization
  loginUser = args.loginUser
  loginPassword = args.loginPassword

  print("----------------------------------------------------")
  print("Adding users to docker hub organization")
  print("Users to add: " + str(users))
  print("Organization: " + organization)
  print("----------------------------------------------------")
  print("")
  token = getToken(loginUser,loginPassword)
  if token is None:
    print("Error getting token")
    exit()
  else:
    add_users(users, organization,token)