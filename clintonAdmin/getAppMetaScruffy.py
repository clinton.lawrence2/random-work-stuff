import psycopg2
import argparse_prompt


def connectoPostGres(appName="",scruffyPass=""):
  scruffyUser = "svc_scruffy"
  scruffyPassword = scruffyPass
  scruffyDbHost="sps-scruffy-api-rds-auroradbclusterscruffyapi-14hlqsb0zps8t.cluster-cbg42zfdthkr.us-west-2.rds.amazonaws.com"
  scruffyDbName = "scruffydb"
  db = psycopg2.connect(dbname=scruffyDbName,
                        user=scruffyUser,
                        host=scruffyDbHost,
                        password=scruffyPassword,
                        port="5432")

  sqlQuery=f'SELECT "name", "fixKey" FROM public.app_metadata WHERE "name" LIKE \'%{appName}%\''
  cursor = db.cursor()
  cursor.execute(sqlQuery)
  rows = cursor.fetchall()
  count = sum([len(t) for t in rows])
  if count == 0:
    print(f"App  with name containing {appName} not found")
  else:
    for table in rows:
      print(table)
  


if __name__ == "__main__":

  #setup parser so we can take command line arguments
  parser = argparse_prompt.PromptParser()
  parser.add_argument('--appName',        help="App Name to look up metadata",type=str)
  parser.add_argument('--dbPass',        help="Scruffy DB Password",type=str)
  args = parser.parse_args()
  appName = args.appName
  dbPass = args.dbPass

  connectoPostGres(appName.upper(),dbPass)



