#stopwatch
import time

def stopwatch():
  start_time = time.time()
  input("Press Enter to stop: ")
  stop_time = time.time()
  print("Time elapsed: ", stop_time - start_time)
        

stopwatch()