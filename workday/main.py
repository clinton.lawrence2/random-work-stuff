#Read CSV
#Alert if dates is today unless weekend day
#if weekend day alert on friday
import csv
from datetime import datetime, timedelta
import tkinter as tk
from tkinter import messagebox
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

scriptPath = os.path.dirname(os.path.realpath(__file__))
fh = logging.FileHandler(scriptPath+'/workday_alert.log')
fh.setFormatter(formatter)
logger.addHandler(fh)

sh = logging.StreamHandler()
sh.setFormatter(formatter)
logger.addHandler(sh)


def sendAlertCheck(date: datetime):
    sendAlert = False
    
    isToday = (date == datetime.now().date())
    isThisSaturday = (date.weekday() == 5 and (date - timedelta(days=1) == datetime.now().date()))
    isThisSunday = (date.weekday() == 6 and (date - timedelta(days=2) == datetime.now().date()))
    
    logger.debug(f"isToday:{isToday} isThisSaturday:{isThisSaturday} isThisSunday:{isThisSunday}")

    if isToday:
        sendAlert = True
    elif isThisSaturday:
        sendAlert = True
    elif isThisSunday:
        sendAlert = True
    return sendAlert

def read_csv_and_alert(csv_file: str):
    dateMap = {}
    with open(csv_file, 'r',encoding='utf-8-sig') as file:
        reader = csv.DictReader(file)
        for row in reader:
            name = row['Name']
            birthDate = row['Birthday']
            birthDate = datetime.strptime(birthDate, '%m-%d').date()
            birthDate = birthDate.replace(year=datetime.now().year)
            serviceDate = row['Service Anniversary']
            serviceDate =  datetime.strptime(serviceDate, '%m-%d-%Y').date()
            yearsOfService = datetime.now().year - serviceDate.year
            compareServiceDate = serviceDate.replace(year=datetime.now().year)
            dates = {"Service Anniversary":compareServiceDate,"Birthday": birthDate, "Years Of Service":yearsOfService }
            dateMap[name] = dates
    alert(dateMap)
           
                   
                    
def alert(dateMap: dict):
    message = f""
    sendAlert = False
    for k,v in dateMap.items():
            name = k
            dates = v
            tmpMsg = f"{name} has"
            for dateType, date in dates.items():
                if dateType == "Years Of Service":
                    continue
                sendAlert = sendAlertCheck(date)
                if sendAlert:
                    logger.info(f"Adding :{tmpMsg} {dateType} on {date}")
                    message += tmpMsg + f" {dateType} on {date}"
                    if dateType == "Service Anniversary":
                        message += f" with {dates["Years Of Service"]} Years of Service\n\n"
                    else:
                        message += "\n\n"
    
    
    logger.info(f"ALERT:{message}")
    root = tk.Tk()
    root.withdraw()  # Hide the main window
    if message == "":
        message = "No one has birthdays or service anniversaries today"
    if datetime.now().weekday() == 4:
        message += " or this weekend"
    messagebox.showinfo("Upcoming Birthdays and Service Anniversaries", message)
    root.destroy()


if __name__ == "__main__":
    logger.info(f"Checking Dates")
    read_csv_and_alert("TeamDates.csv")