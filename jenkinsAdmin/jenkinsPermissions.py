from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import yaml

def find_between(s,first,last):
    try:
      start = s.index(first) + len(first)
      end = s.index(last,start)
      return s[start:end]
    except ValueError:
      return ""

def createFiles(mapToWrite,fileName,mdType):
  markDownString = ""
  print("Creating Markdown List")

  if mdType == "list":
    for group in mapToWrite:
      markDownString +="* "+str(group)
      markDownString += "\n"
      for permission in mapToWrite[group]:
        markDownString +="  * "+str(permission)
        markDownString += "\n"
        if not isinstance(mapToWrite[group], list):
          markDownString +="    * "+str(mapToWrite[group][permission])
          markDownString += "\n"
  
  if mdType == "table":
    markDownString += "| Jira Group | Permission | \n |------------ | ---------- |\n"
    for group in mapToWrite:
      markDownString +=" |"+str(group)+" | <ul>"
      for permission in mapToWrite[group]:
        markDownString += " <li>"+str(permission)+"</li>"
      markDownString += "</ul> | \n"

  print("Writing To Markdown File")
  with open(fileName+'.md', "w") as markDownFile:
    markDownFile.write(markDownString)
    markDownFile.close()


  print("Writing To Yml File")
  with open(fileName+'.yml', 'w') as outfile:
      yaml.dump(mapToWrite, outfile, default_flow_style=False)
      outfile.close

def main():
  chrome_options = Options()
  chrome_options.add_argument("--disable-extensions")
  chrome_options.add_argument("--disable-gpu")
  chrome_options.add_argument("--headless")
  driver = webdriver.Chrome(executable_path="./chromedriver",options=chrome_options)
  baseUrl="https://chisps-jenkins-master.chisps.skytouch.io/"
  driver.get(baseUrl)
  username = driver.find_element_by_id("j_username")
  password = driver.find_element_by_name("j_password")
  username.send_keys("clawrence")
  password.send_keys("s")
  driver.find_element_by_class_name("submit-button").click()
  driver.get(baseUrl+"configureSecurity")
  permTable = driver.find_element_by_class_name("caption-row")
  permissions = permTable.find_elements_by_class_name("pane")
  permission = {}
  skipThese = ["group-row" , "caption-row", "anonymous", "authenticated", "left-most", "stop","User/group","__unused__"]

  tableHeader = driver.find_element_by_id('hudson-security-ProjectMatrixAuthorizationStrategy')

  tableRows = tableHeader.find_elements_by_tag_name("tr")

  userPermissions = {}

  print("Get the User Permissions Mapping...")

  for tableRow in tableRows:
    
    if tableRow.get_attribute("class") in skipThese:
      continue

    name = tableRow.get_attribute("name").replace("[","").replace("]","")
    if name in skipThese:
      continue
    permissionList = []
    permissionEles = tableRow.find_elements_by_tag_name("td")
    for permissionEle in permissionEles:
      if permissionEle.get_attribute("class") in skipThese:
        continue
      if permissionEle.find_element_by_tag_name("input").get_attribute("checked"):
        permissionList.append(permissionEle.find_element_by_tag_name("input").get_attribute("title").split(" for ")[0])
    userPermissions[name] =  permissionList

  headers = tableHeader.find_elements_by_class_name("pane-header")

  groupingPermission = {}
  loopStart = 0
  loopEnd = 0

  print("Get the System Permissions...")
  for header in headers:
    headText = header.text
    if headText in skipThese:
      continue
    headSpan = header.get_attribute("colspan")
    headSpan = int(headSpan)
    loopEnd = loopEnd + headSpan
    for x in range(loopStart, loopEnd):
      title = find_between(permissions[x].get_attribute('innerHTML'),"<span>","</span>")
      description =  permissions[x].get_attribute('title').replace(u"\u2019", "'").replace("\\\"","\"").replace("<br/><br/>"," NOTE:")
      permission[title] = description
    groupingPermission[headText] = permission
    permission = {}
    loopStart = loopEnd

  driver.close()
  createFiles(userPermissions,"UserPermissions","table")
  createFiles(groupingPermission,"SystemDefinedPermissions","list")

main()