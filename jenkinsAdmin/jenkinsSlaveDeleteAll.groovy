import hudson.model.*
import jenkins.model.*


def deleteNodes(String onlyDelete)
{
  Jenkins.instance.slaves.each 
  { workerNode ->
    if (workerNode.name.contains(onlyDelete)) 
    {
     // Assuming workerNode is of type Node
      boolean isJobRunning = workerNode.toComputer().executors.any 
      { Executor executor ->
        executor.isBusy() && executor.currentExecutable != null
      }
      boolean isOffline = workerNode.toComputer().offline
      if (isOffline || !isJobRunning) // either offline or running a jenkins job
      {
        println("Deleting ${workerNode.name}")
        workerNode.toComputer().doDoDelete()
        println("Done Deleting ${workerNode.name}")
      } 
      else 
      {
        println("${workerNode.name} is not safe to delete. Offline:${isOffline}; Running Job: ${isJobRunning}")
      }
    }
  }
}


String onlyDelete = "Ubuntu"

deleteNodes(onlyDelete)