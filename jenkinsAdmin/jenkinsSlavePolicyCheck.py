import boto3

envs = ["int","intdata","qat","qatdata","chiqat","chiqatdata","chilt","prd","prddata","chiprd","chiprddata"]

expAction = "events:PutTargets"

for env in envs:
  containsAction = False
  session=boto3.Session(profile_name=env)
  stsClient = session.client('sts')
 
  response = stsClient.get_caller_identity()
  account = response['Account']

  # print(account)
  iamClient = session.client('iam')
  
  policyArn = "arn:aws:iam::"+account+":policy/"+env+"-jenkins-role"
  policy = iamClient.get_policy(PolicyArn=policyArn)

  # print(policy)
  version = policy['Policy']['DefaultVersionId']

  polyVer = iamClient.get_policy_version(
    PolicyArn=policyArn,
    VersionId=version)

  # print(polyVer)
  for statement in polyVer['PolicyVersion']['Document']['Statement']:
    for action in statement['Action']:
      if action == expAction:
        containsAction = True
  print("Does "+env+" Contains "+ expAction+":"+str(containsAction))
