Clear-Host

$plainPassword = Read-Host -Prompt 'Input your plaintext password'
$plainKey = Read-Host -Prompt 'Input your plain text key'


$secureKey = ""
$securePassword = ""
$encPassword= ""
$secureDecryptedKey = ""
$secureDecryptedPassword = ""
$BSTR1 = ""
$textPassword = ""
$securePassword = ""
$keyCount=0


$keyCount=[System.Text.Encoding]::UTF8.GetByteCount($plainKey)

if ($keyCount  -ne 16 -And $keyCount  -ne 24 -And $keyCount  -ne 32) {
    throw "KEY MUST BE 16,24,or 32 Bytes; your size is $keyCount"
}

$secureKey = ConvertTo-SecureString $plainKey -AsPlainText -Force
$encKey = ConvertFrom-SecureString $secureKey  -Key (1..16)

$securePassword = ConvertTo-SecureString $plainPassword -AsPlainText -Force
$encPassword = ConvertFrom-SecureString $securePassword  -SecureKey $secureKey



$encryptKey=$encKey
$encryptPass=$encPassword

Write-Output "Encrypted values to be used in script:"
Write-Output "`n"
Write-Output "ENCKEY2:$encryptKey"
Write-Output "`n"
Write-Output "ENCPASS2:$encryptPass"
Write-Output "`n"

$testValue= Read-Host -Prompt "Do you want to confirm the encryption works (aka decrypt the password) Y/N"

if ($testValue -like '*y*' -Or $testValue -like '*Y*' )
{

$secureDecryptedKey = ConvertTo-SecureString $encryptKey -Key(1..16)

# $ASTR1 = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureDecryptedKey)
# $textKey = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($ASTR1)

$secureDecryptedPassword = ConvertTo-SecureString $encryptPass -SecureKey $secureDecryptedKey
$BSTR1 = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureDecryptedPassword)
$textPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR1)
Write-Output "UNENCRYPTED PASS:$textPassword"
Write-Output "`n"
}




