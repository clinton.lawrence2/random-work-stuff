import subprocess
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import boto3
import argparse_prompt
import random
import string
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def updateOkta(currentPassword):
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    logging.info("Starting Okta update")
    driver.get(choiceReset)
    logging.info("Wait 5 seconds for window to load")
    time.sleep(5)
    chiUserEl = driver.find_element("id", "input28")
    chiPassEl = driver.find_element("id", "input36")
    chiUserEl.send_keys(CHIlogin)
    chiPassEl.send_keys(currentPassword)
    chiPassEl.submit()
    logging.info("Safe for human to take over (will close window in 2 minutes)")
    time.sleep(120)
    driver.close()
    logging.info("Completed Okta update")

def updateAWSSSO():
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    logging.info("Starting AWS SSO update")
    driver.get("https://skytouchtechnology.awsapps.com/start#/")
    driver.set_window_size(1920, 1055)
    logging.info("Wait 15 seconds for window to load")
    time.sleep(15)
    driver.find_element(By.ID, "awsui-input-0").send_keys("clawrence@skytouchtechnology.com")
    driver.find_element(By.CLASS_NAME, "awsui-button").click()
    logging.info("Wait 15 seconds for window to load")
    time.sleep(15)
    driver.find_element(By.CSS_SELECTOR, "#forgot-password-link span").click()
    logging.info("Safe for human to take over (will close window in 2 minutes)")
    time.sleep(120)

def updateSTAD(currentPassword, newPassword):
    envsToDo = []
    envsDone = []
    logging.info("Fetching ST accounts")
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    driver.get(passordResetSite)
    select = Select(driver.find_element("name", "env"))
    options = select.options
    for option in options:
        if option.get_attribute("value") in envsDone:
            continue
        else:
            envsToDo.append(option.get_attribute("value"))
    driver.close()

    stEnvToSkip = ["chiqa","opdj"]

    logging.info("Updating ST accounts")
    for env in envsToDo:
        if env in stEnvToSkip:  # Skip opendj for now
            logging.info(f"Skipping {env}")
            continue
        driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
        logging.info(f"Updating {env}")
        driver.get(passordResetSite)
        select = Select(driver.find_element("name", "env"))
        select.select_by_value(env)
        loginEl = driver.find_element("name", "login")
        oldPassEl = driver.find_element("name", "oldpassword")
        newPassEl = driver.find_element("name", "newpassword")
        newPassConfEl = driver.find_element("name", "confirmpassword")
        loginEl.send_keys(STlogin)
        oldPassEl.send_keys(currentPassword)
        newPassEl.send_keys(newPassword)
        newPassConfEl.send_keys(newPassword)
        envsDone.append(env)
        time.sleep(3)
        newPassConfEl.submit()
        time.sleep(3)
        driver.close()
        logging.info(f"Completed update for {env}")

def updateAWSIAM(currentPassword, newPassword):
    logging.info("Starting AWS IAM update")
    iamClient = boto3.client('iam')
    response = iamClient.change_password(
        OldPassword=str(currentPassword),
        NewPassword=str(newPassword)
    )
    logging.info("Completed AWS IAM update")

if __name__ == "__main__":
    # Setup parser so we can take command line arguments
    parser = argparse_prompt.PromptParser()

    parser.add_argument('--curPassword', help="Current Password", type=str)
    parser.add_argument('--newPassword', help="New Password", type=str)

    args = parser.parse_args()

    logging.info(args)
    passordResetSite = "https://passwordreset.skytouch.io/index.php?action=change"
    choiceReset = "https://choicehotels.okta.com/enduser/settings"

    STlogin = "clawrence"
    CHIlogin = "clawrence@skytouchtechnology.com"

    curPassword = args.curPassword
    newPassword = args.newPassword

    updateOkta(curPassword)  # Human updates password
    updateAWSSSO()  # No params using forgetpassword link
    updateSTAD(curPassword, newPassword)
    updateAWSIAM(curPassword, newPassword)

    logging.info(f"Done with all accounts. Your new password is: {newPassword}")
