# Dockerfile from image

## How To

The goal of this project is to easily generate a Dockerfile from an existing
Docker image.

To build the image from source:

    cd dockerfile-from-image
    docker build -t dockerfile-from-image .

To get a Dockerfile from an existing image:

* Get docker image Id for the image you want to see

```
SKYM-7J9LVDQ:clintonsstoof clawrenc$ docker images
REPOSITORY                                                             TAG                 IMAGE ID            CREATED             SIZE
749808731048.dkr.ecr.us-west-2.amazonaws.com/dockerfile-from-image     latest              771534f13e1d        14 minutes ago      58.9MB
dockerfile-from-image                                                  latest              771534f13e1d        14 minutes ago      58.9MB
749808731048.dkr.ecr.us-west-2.amazonaws.com/eks/monitoring-appd-new   4.4                 e5a34dc377c3        2 hours ago         1.25GB
ubuntu                                                                 latest              775349758637        2 weeks ago         64.2MB
alpine                                                                 latest              965ea09ff2eb        4 weeks ago         5.55MB
hello-world                                                            latest              fce289e99eb9        10 months ago       1.84kB
```

Run the dockerfile-from-image container passing in the dockerID (Non-Windows Machine)
```
docker run  --rm -v '/var/run/docker.sock:/var/run/docker.sock' dockerfile-from-image $dockerId
```

Run the dockerfile-from-image container passing in the dockerID (Windows Machine)
```
docker run  --rm -v '//var/run/docker.sock:/var/run/docker.sock' dockerfile-from-image $dockerId
```

## Example with the official ubuntu image:

    $ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED
    ubuntu              latest              c73a085dc378        12 days ago

    $ docker run  --rm -v '/var/run/docker.sock:/var/run/docker.sock' dockerfile-from-image c73a085dc378
    FROM ubuntu:latest
    ADD file:cd937b840fff16e04e1f59d56f4424d08544b0bb8ac30d9804d25e28fb15ded3 in /
    RUN /bin/sh -c set -xe 							     \
    	&& echo '#!/bin/sh' > /usr/sbin/policy-rc.d 			     \
    	&& echo 'exit 101' >> /usr/sbin/policy-rc.d 			     \
    	&& chmod +x /usr/sbin/policy-rc.d					\
    	&& dpkg-divert --local --rename --add /sbin/initctl 		\
    	&& cp -a /usr/sbin/policy-rc.d /sbin/initctl 		\
    	&& sed -i 's/^exit.*/exit 0/' /sbin/initctl			\
    	&& echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup			\
    	&& echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean	\
    	&& echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean	\
    	&& echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean 	   			\
    	&& echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages							\
    	&& echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes		\
    	&& echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests
    RUN /bin/sh -c rm -rf /var/lib/apt/lists/*
    RUN /bin/sh -c sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list
    RUN /bin/sh -c mkdir -p /run/systemd \
        && echo 'docker' > /run/systemd/container
    CMD ["/bin/zsh"]

### How Does It Work?

When an image is constructed from a Dockerfile, each instruction in the 
Dockerfile results in a new layer. You can see all of the image layers by
using the `docker images` command with the (soon-to-deprecated) `--tree` flag.

    $ docker images --tree
    Warning: '--tree' is deprecated, it will be removed soon. See usage.
    └─511136ea3c5a Virtual Size: 0 B Tags: scratch:latest
      └─1e8abad02296 Virtual Size: 121.8 MB
        └─f106b5d7508a Virtual Size: 121.8 MB
          └─0ae4b97648db Virtual Size: 690.2 MB
            └─a2df34bb17f4 Virtual Size: 808.3 MB Tags: buildpack-deps:latest
              └─86258af941f7 Virtual Size: 808.6 MB
                └─1dc22fbdefef Virtual Size: 846.7 MB
                  └─00227c86ea87 Virtual Size: 863.7 MB
                    └─564e6df9f1e2 Virtual Size: 1.009 GB
                      └─55a2d383d743 Virtual Size: 1.009 GB
                        └─367e535883e4 Virtual Size: 1.154 GB
                          └─a47bb557ed2a Virtual Size: 1.154 GB
                            └─0d4496202bc0 Virtual Size: 1.157 GB
                              └─5db44b586412 Virtual Size: 1.446 GB
                                └─bef6f00c8d6d Virtual Size: 1.451 GB
                                  └─5f9bee597a47 Virtual Size: 1.451 GB
                                    └─bb98b84e0658 Virtual Size: 1.452 GB
                                      └─6556c531b6c1 Virtual Size: 1.552 GB
                                        └─569e14fd7575 Virtual Size: 1.552 GB
                                          └─fc3a205ba3de Virtual Size: 1.555 GB
                                            └─5fd3b530d269 Virtual Size: 1.555 GB
                                              └─6bdb3289ca8b Virtual Size: 1.555 GB
                                                └─011aa33ba92b Virtual Size: 1.555 GB Tags: ruby:2, ruby:2.1, ruby:2.1.1, ruby:latest

Each one of these layers is the result of executing an instruction in a
Dockerfile. In fact, if you do a `docker inspect` on any one of these layers
you can see the instruction that was used to generate that layer.

    $ docker inspect 011aa33ba92b
    [{
      . . .
      "ContainerConfig": {
        "Cmd": [
            "/bin/sh",
            "-c",
            "#(nop) ONBUILD RUN [ ! -e Gemfile ] || bundle install --system"
        ],
        . . .
    }]

The output above has been truncated, but nested within the *ContainerConfig* 
data you'll find the Dockerfile command that generated this layer (in this case
it was an `ONBUILD` instruction).

The *dockerfile-from-image* script works by simply walking backward through the
layer tree and collecting the commands stored with each layer. When the script
reaches the first tagged layer (or the root of the tree) it stops and displays
the (reversed) list of commands. If you want to generate the commands going
all the way back to the root image layer you can use the `-f` flag to walk the
entire tree.

### Limitations
As the *dockerfile-from-image* script walks the list of layers contained in the
image it stops when it reaches the first tagged layer. It is assumed that a layer
which has been tagged represents a distinct image with its own Dockerfile so the
script will output a `FROM` directive with the tag name.

In the example above, the *ruby* image contained a layer in the local image
repository which had been tagged with *buildpack-deps* (though it wasn't shown
in the example, this likely means that *buildpack-deps:latest* was also pulled
at some point). If the *buildpack-deps* layer had not been tagged, the
*dockerfile-from-image* script would have continued outputing Dockerfile
directives until it reached the root layer.

Also note that the output generated by the script won't match exactly the
original Dockerfile if either the `COPY` or `ADD` directives (like the
example above) are used. Since we no longer have access to the build context
that was present when the original `docker build` command was executed all we
can see is that some directory or file was copied to the image's filesystem
(you'll see the file/directory checksum and the destination it was copied to).