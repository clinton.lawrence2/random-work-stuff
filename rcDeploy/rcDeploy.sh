#! zsh
#Open CMs
echo "open CMs"
cmFile="/Users/clawrence/repos/clintonsstoof/awsScripts/rcDeploy/cm.txt"
for cm in "${(@f)"$(<$cmFile)"}"
  do
    echo $cm
    open -a /Applications/Google\ Chrome.app "$cm"
  done
read -s -k '?Press any key to continue.'
echo "\n"

#OpenPRs
echo "open PRs"
prsFile="/Users/clawrence/repos/clintonsstoof/awsScripts/rcDeploy/prs.txt"
for pr in "${(@f)"$(<$prsFile)"}"
  do
    echo $pr
    open -a /Applications/Google\ Chrome.app "$pr"
  done
read -s -k '?Press any key to continue.'
echo "\n"

#Open AWS Console
open -a /Applications/Authy\ Desktop.app/
open -a /Applications/Google\ Chrome.app "https://aws.amazon.com/console"
read -s -k '?Press any key to continue.'
open -a /Applications/Google\ Chrome.app "https://signin.aws.amazon.com/switchrole?account=choiceprd&roleName=skyAdmin&displayName=skyAdmin:CHI-PRD&color=99BCE3"
echo "\n"

#Get Active Passive Stacks
rcFamily=("HOS-RC" "HOS-RC-APACHE" "HOS-RC-RAVEN" "HOS-RC-SVC" "HOS-RC-SVC-API")
for rcApp in "$rcFamily[@]";
  do 
    if read -q "doIt?Are we deploying $rcApp? " ; then
      echo "\n"
      echo "Getting ASGs for $rcApp"
      asgA=$(aws cloudformation list-stack-resources --stack-name PRD2-$rcApp-EC2-A --query "StackResourceSummaries[?ResourceType=='AWS::AutoScaling::AutoScalingGroup'].PhysicalResourceId" --output text --profile chiprd)
      asgB=$(aws cloudformation list-stack-resources --stack-name PRD2-$rcApp-EC2-B --query "StackResourceSummaries[?ResourceType=='AWS::AutoScaling::AutoScalingGroup'].PhysicalResourceId" --output text --profile chiprd)
      desiredA=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names $asgA --query 'AutoScalingGroups[*].DesiredCapacity' --output text --profile chiprd)
      desiredB=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names $asgB --query 'AutoScalingGroups[*].DesiredCapacity' --output text --profile chiprd)
      active="NONE FOUND"
      passive="NONE FOUND"
      if [[ desiredB -eq 0 && desiredA -gt 0 ]];then
          active=$asgA
          passive=$asgB
      elif [[ desiredB -gt 0 && desiredA -eq 0 ]] ; then
          active=$asgB
          passive=$asgA
      fi
      echo "ACTIVE $rcApp IS $active"
      open -a /Applications/Google\ Chrome.app "https://us-west-2.console.aws.amazon.com/ec2/home?region=us-west-2&redirectFrom=asg#AutoScalingGroupDetails:id=$active;view=details"
      echo "PASSIVE $rcApp IS $passive"
      open -a /Applications/Google\ Chrome.app "https://us-west-2.console.aws.amazon.com/ec2/home?region=us-west-2&redirectFrom=asg#AutoScalingGroupDetails:id=$passive;view=details"
    else
      echo "\n"
      echo "skipping $rcApp"
      echo "\n"
    fi
  done
read -s -k '?Press any key to continue.'
echo "\n"

#JMS CONSUMPTION VERIFICATION
echo "open SS for RC"
open -a /Applications/Google\ Chrome.app "https://ss.skytouch.io/app/#/secret/3219/general"
read -s -k '?Press any key to continue.'
winJumpIp=$(aws ec2 describe-instances --filters 'Name=instance-state-name,Values=running' 'Name=tag:Name,Values=jump*' --query 'Reservations[0].Instances[?Platform==`windows`].[PrivateIpAddress]' --profile chisps --output text)
username="clawrence@chiprd.skyad.io"
open -a /Applications/Microsoft\ Remote\ Desktop.app "rdp://full address=s:$winJumpIp&username=s:$username"

