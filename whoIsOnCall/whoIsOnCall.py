import requests
from datetime import datetime, timedelta
import pytz

# Set your Opsgenie API key and schedule name
API_KEY = 'd3dd70f3-41e6-4333-b43f-2a12647cb49a'
SCHEDULE_NAME = 'SysEngineer_schedule'

# Opsgenie API endpoint URLs
BASE_URL = 'https://api.opsgenie.com/v2/'
SCHEDULES_URL = BASE_URL + 'schedules'

def get_schedule_id(schedule_name):
    headers = {
        'Authorization': 'GenieKey ' + API_KEY,
        'Content-Type': 'application/json'
    }

    response = requests.get(SCHEDULES_URL, headers=headers)
    data = response.json()

    for schedule in data['data']:
        if schedule['name'] == schedule_name:
            return schedule['id']

    return None

def get_on_call(schedule_id, start_date, interval, interval_unit):
    headers = {
        'Authorization': 'GenieKey ' + API_KEY,
        'Content-Type': 'application/json'
    }

    params = {
        'date': start_date.strftime('%Y-%m-%dT%H:%M:%SZ'),
        'interval': interval,
        'intervalUnit': interval_unit,
        'expand': 'base'
    }

    schedule_url = f"{SCHEDULES_URL}/{schedule_id}/timeline"

    response = requests.get(schedule_url, params=params, headers=headers)
    data = response.json()

    on_call_list = []

    if 'data' in data and 'finalTimeline' in data['data']:
        for rotation in data['data']['finalTimeline']['rotations']:
            for period in rotation['periods']:
                on_call = period.get('recipient')
                if on_call and on_call['type'] == 'user':
                    user_id = on_call['id']
                    user_name = on_call['name']
                    start = period['startDate']
                    end = period['endDate']
                    on_call_list.append({'user_id': user_id, 'user_name': user_name, 'start': start, 'end': end})
    else:
        print("Error fetching on-call data from Opsgenie API.")
        print("Response:", data)

    return on_call_list

def print_on_call(on_call_list):
    for entry in on_call_list:
        print(f"On call: {entry['user_name']} ({entry['user_id']}) from {entry['start']} to {entry['end']}")

if __name__ == '__main__':
    schedule_id = get_schedule_id(SCHEDULE_NAME)

    if schedule_id is None:
        print(f"Schedule '{SCHEDULE_NAME}' not found.")
    else:
        # Get the local timezone
        local_tz = pytz.timezone('America/Phoenix')  # replace with your local timezone

        # Get the current local time
        today = datetime.now(local_tz)

        # Calculate the start of the current week (Monday 00:00:00)
        start_of_week = today - timedelta(days=today.weekday()) + timedelta(days=1)
        start_of_week = start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)

        # Calculate the start of next week (Monday 00:00:00)
        start_of_next_week = start_of_week + timedelta(days=7)

        # Calculate the start of two weeks from now (Monday 00:00:00)
        start_of_two_weeks_later = start_of_week + timedelta(days=14)

        print(f"On call for Week Of {start_of_week}")
        on_call_this_week = get_on_call(schedule_id, start_of_week, 1, 'weeks')
        print_on_call(on_call_this_week)

        print(f"\nOn call for Week Of {start_of_next_week}")
        on_call_next_week = get_on_call(schedule_id, start_of_next_week, 1, 'weeks')
        print_on_call(on_call_next_week)

        print(f"\nOn call for Week Of {start_of_two_weeks_later}")
        on_call_two_weeks_later = get_on_call(schedule_id, start_of_two_weeks_later, 1, 'weeks')
        print_on_call(on_call_two_weeks_later)
