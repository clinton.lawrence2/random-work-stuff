# Ansible Role Inventory
This script can be ran to grab all our playbooks and generate 4 files as it relates to our automation and ansible role usage.

* roles.csv -- for getting a role to playbook matrix
* appRole.csv -- for getting an app(playbook) to role matrix
* customPlaybooks.csv -- what apps used custom playbooks
* genericSpringBoot.csv -- what apps used generic springboot playbook

## Preqs
* Python3 -- https://www.python.org/downloads/
* GitPython Module -- use the requirements file
    ``` 
    pip3 install -r requirements.txt 
    ```

## How to
To use simply run the role inventory script
```
./roleInventory.py --bitbucket-user clawrence --bitbucket-password MYSUPERCOOLPASSWORD!
```
This will clone the deployment repo, and get app list from build_config,deployment_config AND st-infra and then use them to make the files mentioned above

## Files
There are multiple that either exist now or will exist after the script is Ran

### appRole.csv (Built by script)
This file will story a matrix of playbook to list of roles use

### requirements.txt
This is a config file that can be used by python to install required libraries

### Role.py
A python class use to hold the object of a role

The object has 3 attributes
* name -- name of the role
* playbook -- what playbook(s) is this role used in
* repo -- what is the URL of the role in BB

### roleInventory.py (Main script)
This is the script that generates the csv files

### roles.csv (Built by script)
This file will store all roles and what playbook they are in

### customPlaybooks.csv
This file will store all apps that have a custom playbook

### genericSpringBoot.csv
This file will store all apps that use the generic spring book playbook