class Role:
    def __init__(self, name,playbookDir,repoUrl):
        self.__name = name
        self.__repo = repoUrl
        self.__playbookDir = playbookDir


    
    @property    
    def name(self):
        return self.__name
        
    @name.setter    
    def setName(self,value):
        self.__name = value
    
    @property    
    def repo(self):
        return self.__repo

    @repo.setter    
    def repo(self,value):
        self.__repo = value

    @property    
    def playbookDir(self):
        return self.__playbookDir

    @playbookDir.setter    
    def playbookDir(self,value):
        self.__playbookDir = value