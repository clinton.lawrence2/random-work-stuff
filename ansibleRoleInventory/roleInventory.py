#!/usr/bin/env python3
import csv
import logging
import os
import shutil
from collections import defaultdict

import git
import requests
import yaml
import argparse

from Role import *


def buildCustomPlaybookFile(appList):
     with open('customPlayBooks.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(["AppName"])
        for app in appList:
            spamwriter.writerow([app])

def buildGenericPlaybookFile(appList):
    with open('genericSpringBoot.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(["AppName"])
        for app in appList:
            spamwriter.writerow([app])

def buildRoleListFile(roleDict):
    
    with open('roles.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(["Role","Playbook-dir","Repo"])
        for role in roleDict.values():
            spamwriter.writerow([role.name,role.playbookDir,role.repo])

def buildAppRoleListFile(appRoleDict):
    with open('appRole.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(["App","roles"])
        for app in appRoleDict:
            roles = str(appRoleDict[app])
            spamwriter.writerow([app, roles])

def cleanupUrl(repoUrl):
    repoUrl=repoUrl.replace("ssh://git@","https://")
    repoUrl=repoUrl.replace("git+https://","https://")
    repoUrl=repoUrl.replace("git+http://","https://")
    repoUrl=repoUrl.replace("ansbl","projects/ANSBL/repos")
    repoUrl=repoUrl.replace(".git","")
    return repoUrl

def sortApp(app,deploymentList):
    logging.debug("Sorting:" + app)
    
    hasCustomPlaybook = False
    

    if app in deploymentList:
        logging.debug(app + " has custom playbook")
        hasCustomPlaybook = True

    return hasCustomPlaybook

def sortAppList(appList,deploymentList):

    appCustomDict = {}
    for app in appList:
        hasCustomPlaybook = sortApp(app,deploymentList)
        appCustomDict[app] = hasCustomPlaybook
    
    return appCustomDict


def buildAppPlaybookFile(user,password):
    customPlaybookDict = []
    genericSpringBootDict = []

    hosDeployUrl = 'https://scm.skytouch.io/projects/DEPLOY/repos/hos/raw/deployment_config.json'
    
    hosBuildUrl = 'https://scm.skytouch.io/projects/DEPLOY/repos/hos/raw/build_config.json'
    
    appListHosBuild = requests.get(url = hosBuildUrl,auth=(user,password)).json()["apps"].keys()
    appsListStInfra = [name for name in os.listdir('./tmp-stinfra/st-infra/cloudformation/automation')if os.path.isdir(os.path.join('./tmp-stinfra/st-infra/cloudformation/automation', name))]
    appListHosDeploy = requests.get(url = hosDeployUrl,auth=(user,password)).json()["apps"].keys()

    appsListPlaybooks = [name for name in os.listdir('./tmp-deployment-repo/deployment')if os.path.isdir(os.path.join('./tmp-deployment-repo/deployment', name))]
    
    
    bigAppList = list(set().union(appListHosBuild, appsListStInfra, appListHosDeploy))

    bigAppList.sort()

    logging.debug("ALL APP LIST:" +str(bigAppList))

    appListSorted = sortAppList(bigAppList,appsListPlaybooks)

    for key, value in appListSorted.items() :
        if value:
            customPlaybookDict.append(key)
        else:
            genericSpringBootDict.append(key)

    

    genericSpringBootDict.sort()
    customPlaybookDict.sort()
    buildGenericPlaybookFile(genericSpringBootDict)
    buildCustomPlaybookFile(customPlaybookDict)

def buildAppRoleList():
    roleDict = {}
    appRoleDict = {}
    playbookFile='requirements.yml'
    path='./tmp-deployment-repo/deployment/$app/ansible/roles'
    alreadyAddedRole=[]
    apps=[name for name in os.listdir('./tmp-deployment-repo/deployment')if os.path.isdir(os.path.join('./tmp-deployment-repo/deployment', name))]

    for app in apps:
        if (app == ".git") or app == ".vscode" :
            continue
        logging.info('Getting Roles for '+ app)
        tmpRoleList = []
        appPath=path.replace('$app',app)
        filePath = os.path.join(appPath, playbookFile)
        requirementsData=yaml.load(open(filePath),Loader=yaml.FullLoader)
        for reqRole in requirementsData:
            tmpRoleList.append(reqRole["name"])
            if reqRole["name"] in alreadyAddedRole:
                logging.debug(reqRole["name"]+ " exists going to next one")
                roleDict.get(reqRole["name"]).playbookDir = roleDict.get(reqRole["name"]).playbookDir +','+app
                continue
            logging.debug('Adding Role:'+reqRole["name"]+" From App:"+app)
            alreadyAddedRole.append(reqRole["name"])
            repoUrl=cleanupUrl(reqRole["src"])
            role = Role(reqRole["name"],app,repoUrl)
            roleDict[reqRole["name"]]=role
        tmpRoleList.sort()
        appRoleDict[app]=tmpRoleList
        buildRoleListFile(roleDict)
        buildAppRoleListFile(appRoleDict)

def gitCheckout(path,url):
    
    if os.path.isdir(path):
        logging.info("Deleting old copy of Dir")
        shutil.rmtree(path)
    
    os.mkdir(path)

    git.Git(path).clone(url)

def clearTmpDirs():
    
    logging.info("Deleting Dirs")
    shutil.rmtree('./tmp-deployment-repo')
    shutil.rmtree('./tmp-stinfra')

def main(args):
    
    logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.DEBUG)
    gitCheckout('./tmp-deployment-repo','ssh://git@scm.skytouch.io/deploy/deployment.git')
    gitCheckout('./tmp-stinfra','ssh://git@scm.skytouch.io/infra/st-infra.git')
    buildAppRoleList()
    buildAppPlaybookFile(args.bitbucket_user,args.bitbucket_password)
    clearTmpDirs()

if __name__== "__main__":
    
    #setup parser so we can take command line arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("-u",'--bitbucket-user',            required=True ,     help="Pass in bitbucket username to grab configs")
    parser.add_argument("-p",'--bitbucket-password',        required=True ,     help="Pass in bitbucket password to grab configs")

    args = parser.parse_args()
    main(args)