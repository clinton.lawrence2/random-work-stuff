ENV=$1
AppEnvId=$2

if [[ -z $AppEnvId  ]];then
    echo "Not AppEnvId Provided using wildcard"
    AppEnvId="*"
fi

aws ec2 describe-instances \
--filters "Name=tag:app_environment_id,Values=${AppEnvId}" \
--query "Reservations[].Instances[].{APPEnvId:Tags[?Key=='app_environment_id']|[0].Value,APPName:Tags[?Key=='Name']|[0].Value,NexusVersion:Tags[?Key=='nexus_version']|[0].Value}" --profile $ENV --output table | uniq