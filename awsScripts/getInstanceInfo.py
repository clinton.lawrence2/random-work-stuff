import boto3

def get_ec2_instance_info(instance_id, profile_name):
    session = boto3.Session(profile_name=profile_name)
    ec2_client = session.client('ec2')
    response = ec2_client.describe_instances(InstanceIds=[instance_id])
    
    if 'Reservations' in response and len(response['Reservations']) > 0:
        instance = response['Reservations'][0]['Instances'][0]
        instance_info = {
            'InstanceID': instance['InstanceId'],
            'InstanceType': instance['InstanceType'],
            'LaunchTime': instance['LaunchTime'].strftime("%Y-%m-%d %H:%M:%S"),
            'State': instance['State']['Name'],
            'PublicIP': instance.get('PublicIpAddress', 'N/A'),
            'PrivateIP': instance['PrivateIpAddress']
        }
        return instance_info
    else:
        return None
    
def find_instance_across_accounts(instance_id, account_ids):
    instance_info = None
    accountIDFound = ""
    
    for account_id in account_ids:
        try:
            print(f"looking in {account_id}")
            instance_info = get_ec2_instance_info(instance_id, account_id)
            if instance_info:
                accountIDFound = account_id
                break
        except Exception as e:
            if str("InvalidInstanceID.NotFound" in str(e)):
                print(f"{instance_id} does not exist in {account_id}")
            else:
              print(f"Error occurred while retrieving instance info from account {account_id}: {str(e)}")
    
    return instance_info,accountIDFound


if __name__ == '__main__':
    instance_id = input("Enter the EC2 instance ID: ")
    envs = ["int","qat","chiqat","chilt","sps","chisps","prd","chiprd"]
    
    instance_info = find_instance_across_accounts(instance_id, envs)
    if instance_info[0]:
        print(f"Instance found in {instance_info[1]}:")
        print(instance_info[0])
    else:
        print("Instance not found across all accounts.")

