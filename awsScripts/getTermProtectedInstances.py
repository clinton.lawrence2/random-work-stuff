#Get termination protection attribute for given ec2 instances
import boto3
import datetime

ec2 = boto3.client('ec2')

def calaculateAgeOfInstance(launchTime):

  now = datetime.datetime.now()
  launchTime = launchTime.replace(tzinfo=None)
  diff = now - launchTime
  return diff

def getTermProtectedInstance(instanceId,instanceName):
  response = ec2.describe_instance_attribute(
    Attribute= "disableApiTermination",
    InstanceId = instanceId
  )

  return (response['DisableApiTermination']['Value'] == True)

# get instances where name tag contains Ubuntu Jenkins Slave
instances = ec2.describe_instances(
  Filters=[
    {
      'Name': 'tag:Name',
      'Values': ['Ubuntu Jenkins Slave*']
    }
  ]
)

for reservation in instances['Reservations']:
  for instance in reservation['Instances']:
    #get values of name tag for each instance
    instanceName=""
    for tag in instance['Tags']:
      if tag['Key'] == 'Name':
        instanceName = tag['Value']
    termProtection = getTermProtectedInstance(instance['InstanceId'],instanceName)
    #convert launch time to datetime object
    launchTime = instance['LaunchTime']
    #calculate age of instance
    age = calaculateAgeOfInstance(launchTime=launchTime)
    old = False
    #is age greater than 1 day if so set "old" to true
    if age > datetime.timedelta(days=1):
      old = True
    if not termProtection and old:
      #print instance details
      print("Instance Name: ", instanceName)
      print("Instance Id: ", instance['InstanceId'])
      print("Termination Protection: ", termProtection)
      print("Age of Instance: ", age)
      print("Old: ", old)
      print(" ")