import boto3
import jmespath

resultQuery = jmespath.compile("Contents[*].Key")

session=boto3.Session(profile_name="sps")
s3Client = session.client('s3')

s3Bucket = "st-app-meta"
pipelineType = "LOGI"
response = s3Client.list_objects_v2(Bucket=s3Bucket)

filterResponse = resultQuery.search(response)
for s3Object in filterResponse:
  if(".yml" in s3Object and pipelineType in s3Object):
    newS3pieces = s3Object.rsplit('/', 1)
    s3FileName = newS3pieces[len(newS3pieces)-1]
    destination = pipelineType+"/"+s3FileName
    print("Copy "+s3Object+" to "+destination)
    s3Client.copy_object(
      Bucket=s3Bucket,
      CopySource="/"+s3Bucket+"/"+s3Object,
      Key=destination,
    )


