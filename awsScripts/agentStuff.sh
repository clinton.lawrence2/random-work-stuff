name: ecs-agent
    image: 749808731048.dkr.ecr.us-west-2.amazonaws.com/amazon/amazon-ecs-agent:latest
    state: started
    net: host
    privileged: yes
    pull: always
    restart_policy: always
    volumes:
    - /var/run:/var/run
    - /var/log/ecs/:/log
    - /var/lib/ecs/data:/data
    - /etc/ecs:/etc/ecs
    env:
        ECS_DATADIR: /data
        ECS_ENABLE_TASK_IAM_ROLE: true
        ECS_ENABLE_TASK_IAM_ROLE_NETWORK_HOST: true
        ECS_LOGFILE: /log/ecs-agent.log
        ECS_AVAILABLE_LOGGING_DRIVERS: "[\"awslogs\",\"fluentd\",\"gelf\",\"json-file\",\"journald\",\"splunk\",\"logentries\",\"syslog\"]"
        ECS_LOGLEVEL: info
        ECS_CLUSTER: "{{ app_environment_id }}-ecs-cluster"
				


docker run -d --name ecs-agent --net host --privileged --restart=always -v /var/run:/var/run -v /var/log/ecs/:/log -v /var/lib/ecs/data:/data -v /etc/ecs:/etc/ecs -e ECS_DATADIR=/data -e ECS_ENABLE_TASK_IAM_ROLE=true -e ECS_LOGFILE=/log/ecs-agent.log -e ECS_AVAILABLE_LOGGING_DRIVERS="[\"awslogs\",\"fluentd\",\"gelf\",\"json-file\",\"journald\",\"splunk\",\"logentries\",\"syslog\"]" -e ECS_LOGLEVEL=info ECS_CLUSTER="int-taurus-ecs-cluster" 749808731048.dkr.ecr.us-west-2.amazonaws.com/amazon/amazon-ecs-agent:latest

docker stop ecs-agent && docker rm ecs-agent && docker run --name=ecs-agent --hostname=ecs-cluster-ip-10-65-169-17 --env=ECS_LOGLEVEL=info --env=ECS_CLUSTER=int-taurus-ecs-cluster --env=ECS_ENABLE_TASK_IAM_ROLE_NETWORK_HOST=True --env='ECS_AVAILABLE_LOGGING_DRIVERS=["awslogs","fluentd","gelf","json-file","journald","splunk","logentries","syslog"]' --env=ECS_ENABLE_TASK_IAM_ROLE=True --env=ECS_DATADIR=/data --env=ECS_LOGFILE=/log/ecs-agent.log --env=PATH=/host/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin --volume=/var/lib/ecs/data:/data:rw --volume=/var/log/ecs/:/log:rw --volume=/var/run:/var/run:rw --volume=/etc/ecs:/etc/ecs:rw --volume=/data --volume=/etc/ecs --volume=/log --volume=/var/run --network=host --privileged --restart=always --runtime=runc --detach=true 749808731048.dkr.ecr.us-west-2.amazonaws.com/amazon/amazon-ecs-agent:latest