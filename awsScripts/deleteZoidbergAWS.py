import boto3
import logging

logging.basicConfig(format = '%(asctime)s %(levelname)s:%(message)s',datefmt = '%Y-%m-%d %H:%M:%S',level = logging.INFO)
logging.getLogger('boto3').setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.INFO)
logging.getLogger('requests').setLevel(logging.INFO)

def getAppStacks(cfnClient,appName):
  validationFilters = ['CREATE_COMPLETE','ROLLBACK_COMPLETE','UPDATE_COMPLETE','UPDATE_ROLLBACK_COMPLETE','DELETE_FAILED']
  appStacks = []
  resourcesToSkip = ["KMS","SG","SG-RULES"]
  stacks = cfnClient.list_stacks(StackStatusFilter=validationFilters)
  listOfStacks = stacks["StackSummaries"]
  cfnNextToken = "Done"
  if ("NextToken" in stacks):
    cfnNextToken = stacks["NextToken"]
    while cfnNextToken != None:
      for stack in listOfStacks:
          if stack['StackStatus'] == 'DELETE_COMPLETE':
              logging.debug("stack we found was just deleted moving on")
              continue
          if appName.upper() in stack['StackName']:
              if any(res.upper() in stack['StackName'].upper() for res in resourcesToSkip):
                continue
              appStacks.append(stack['StackName'])

      if cfnNextToken != "Done":
          stacks = cfnClient.list_stacks(NextToken=cfnNextToken,StackStatusFilter=validationFilters)
          listOfStacks = stacks["StackSummaries"]
          if ("NextToken" in stacks):
              cfnNextToken = stacks["NextToken"]
          else:
              cfnNextToken = None
      else:
              cfnNextToken = None       
  
  return appStacks


appName = "zoidberg"
envs = ["int","qat","chiqat","prd","chiprd"]

for env in envs:
  logging.info("Getting "+env+" Stacks")
  session=boto3.Session(profile_name=env)
  cfnClient = session.client('cloudformation')

  appStacks = getAppStacks(cfnClient,appName)
  for appStack in appStacks:
    logging.info("Deleting "+appStack)
    # cfnClient.delete_stack(StackName=appStack)
    # waiter = cfnClient.get_waiter('stack_delete_complete')
    # logging.info("Waiting for stack "+ appStack+" to be deleted...")
    # waiter.wait(StackName=appStack)