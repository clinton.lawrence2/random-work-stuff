LATESTAMI=$1
PROFILE=$2

bail_on_error(){
# args 1=return code, 2=success_message, 3=fail_message
    if [ $1 != 0 ]; then
            exit 1
    fi
}

aws ec2 wait image-exists --image-ids $LATESTAMI --profile sps
bail_on_error $?

echo "~~~~~~~~~~Current Running Instances~~~~~~~~~~~~~~~~"

aws ec2 describe-instances \
--query "Reservations[].Instances[?(ImageId != '${LATESTAMI}' && Platform != 'windows') ].[Tags[?Key=='Name'] | [0].Value, ImageId, InstanceId]" \
--output text \
--profile ${PROFILE}

echo "~~~~~~~~~~END Current Running Instances~~~~~~~~~~~~~~~~"

aws autoscaling describe-launch-configurations \
--query "LaunchConfigurations[?(ImageId != '${LATESTAMI}')].{LaunchConfigName:LaunchConfigurationName, AMI:ImageId}" \
--output table \
--profile ${PROFILE}