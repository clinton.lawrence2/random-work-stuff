$LATESTSDBs=""

$BUCKET="st-infra-int-mssql-backups"
$PREFIX="MSSQL/Backups/"
$RBKEYCOMMAND="aws s3api list-objects-v2 --bucket $BUCKET --prefix $PREFIX --query 'reverse(sort_by(Contents,&LastModified))[?contains(Key,``rb``)] | [0].Key' --profile dev --output text"
$RBKEY = Invoke-Expression $RBKEYCOMMAND

$latestRBVer=$RBKEY.Split("MSSQL/Backups/apache_readonly_2012_rb_")[1]

Write-Output "RB VER:$latestRBVer"

$LATESTSDBs+=$RBKEY
$LATESTSDBs+=" " 

$PMKEYCOMMAND="aws s3api list-objects-v2 --bucket $BUCKET --prefix $PREFIX --query 'reverse(sort_by(Contents,&LastModified))[? ! contains(Key,``rb``) && ! contains(Key,``rpm``)] | [0].Key' --profile dev --output text"
$PMKEY = Invoke-Expression $PMKEYCOMMAND

$latestPMVer=$RBKEY.Split("MSSQL/Backups/apache_readonly_2012_")[1]

Write-Output "PM VER:$latestPMVer"

$LATESTSDBs+=$PMKEY

Write-Output "LATESTDBS:"
$LATESTSDBs