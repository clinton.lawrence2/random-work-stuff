import boto3
import botocore
import os
import webbrowser
import subprocess

tagToCheck="zoidberg"
cfnList = []  

profileList=["int","qat","sps","prd","chiqat","chilt","chisps","chiprd"]
for profile in profileList:
    print ("~~~~~~~~~~~~~~~~~~~~~~Start searching of "+profile+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    session=boto3.Session(profile_name=profile)
    # s3Resource = session.resource('s3')
    # s3Client = session.client('s3')
    # ec2Client = session.client('ec2')
    cfnResource = session.resource('cloudformation')
    # asgClient = session.client('autoscaling')
    # termEc2List=[]
    # termAsgList=[]
    # stopEc2List=[]
    # cleanS3List=[]
    # delS3List=[]
   
    for stack in cfnResource.stacks.all():
        stackNameLowered=stack.stack_name.lower()
        if tagToCheck in stackNameLowered :
            cfnList.append(stackNameLowered)
            stackObj=cfnResource.Stack(stack.stack_name)
            # stackObj.delete()
    print ("~~~~~~~~~~~~~~~~~~~~~~Done Checking cfn for "+profile+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

cfnList.sort()
print(cfnList)
#     print ("~~~~~~~~~~~~~~~~~~~~~~Checking s3 for "+profile+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
#     for bucket in s3Resource.buckets.all():
#         bucketNameLowered=bucket.name.lower()
#         if bucketNameLowered != "clinton-int-test-buckets-ok":    
#             if tagToCheck in bucketNameLowered :
#                     print (bucketNameLowered)
#                     action = input("What do you want to do to the Bucket? Clean/Delete/Nothing: ")
#                     if action.upper() == "CLEAN":
#                         cleanS3List.append(bucketNameLowered)
#                     elif action.upper() == "DELETE":
#                         cleanS3List.append(bucketNameLowered)
#                         delS3List.append(bucketNameLowered)
    
#     if len(cleanS3List) > 0:
#         print ("Attempting to clean s3 buckets:"+ str(cleanS3List))
#         for bucketName in cleanS3List:
#             bucket = s3Resource.Bucket(bucketName)
#             areWeVersioning=""
#             try:
#                 response = s3Client.get_bucket_versioning(Bucket=bucketName)
#                 areWeVersioning=response['Status']
#             except KeyError:
#                 areWeVersioning="Suspended"
#                 delS3List.append(bucketName)
#             except s3Client.exceptions.NoSuchBucket:
#                 areWeVersioning="None"
#                 delS3List.append(bucketName)

#             if areWeVersioning == "Enabled":
#                 bucket.object_versions.delete()
#             elif areWeVersioning == "Suspended":
#                 bucket.objects.all().delete()
#         del cleanS3List[:]
    
#     if len(delS3List) > 0:
#         print ("deleting s3 buckets:"+ str(delS3List))
#         for bucketName in delS3List:
#             try:
#                 bucket = s3Resource.Bucket(bucketName)
#                 bucket.delete()
#             except s3Client.exceptions.NoSuchBucket:
#                 print ("bucket: "+ str(bucketName) + " already removed")
#         del delS3List[:]
    
#     print ("~~~~~~~~~~~~~~~~~~~~~~Checking ASG for "+profile+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
#     asgs = asgClient.describe_auto_scaling_groups()
#     for asg in asgs["AutoScalingGroups"]:
#         asgNameLowered=asg["AutoScalingGroupName"].lower()
#         if tagToCheck in asgNameLowered :
#             print (asgNameLowered)
#             action = input("What do you want to do to the Instance? Delete/Nothing: ")
#             if action.upper() == "DELETE":
#                 print ("Deleting " + asgNameLowered)
#                 asgClient.delete_auto_scaling_group(AutoScalingGroupName=asgNameLowered,ForceDelete=True)
    


#     print ("~~~~~~~~~~~~~~~~~~~~~~Checking ec2 for "+profile+" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
#     response = ec2Client.describe_instances( Filters=[{'Name': 'tag:Name','Values': ['*clinton*'] }] )

#     for reservation in response['Reservations']:
#         for instance in reservation["Instances"]:
#             tagDict=instance['Tags']
#             for tag in tagDict:
#                 if str(tag['Key']) == "Name":
#                     instanceId=instance['InstanceId']
#                     instanceName=str(tag['Value'])
#                     instanceState=str(instance['State']['Name'])
#                     print ("InstanceID:"+ instanceId + " Name:" + instanceName + " State:" + instanceState )
#                     if instanceState != "terminate":
#                         action = input("What do you want to do to the Instance? Stop/Terminate/Nothing: ")
#                         if action.upper() == "STOP":
#                             stopEc2List.append(instanceId)
#                         elif action.upper() == "TERMINATE":
#                             termEc2List.append(instanceId)

                    
    
#     if len(stopEc2List) > 0:
#         print ("stopping instances:"+ str(stopEc2List))
#         response = ec2Client.stop_instances(InstanceIds=stopEc2List)
#         del stopEc2List[:]
#     if len(termEc2List) > 0:
#         print ("terminating instances:"+ str(termEc2List))
#         response = ec2Client.terminate_instances(InstanceIds=termEc2List)
#         del termEc2List[:]
#     print ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Done with "+profile+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

# input("Grab Headphones...press enter to continue....")
