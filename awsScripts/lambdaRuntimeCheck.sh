profiles="int qat chiqat chilt prd chiprd"
runtimeToCheck="java8"

printf "" > lambdas.txt

for prof in $profiles; do
  printf "\nFunctions for $prof\n\n" >> lambdas.txt
	aws lambda list-functions  --function-version ALL --query 'Functions[?Runtime==`'$runtimeToCheck'`].{FunctionName:FunctionName}' --output table --profile $prof >> lambdas.txt
  printf "\n\n" >> lambdas.txt
done
