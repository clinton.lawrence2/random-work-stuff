envLower="$1"
services="bitbucket nexus apt-mirror"

URLPreDomainBB="scm"
foundBBService="false"

URLPreDomainNexus="nexus"
foundNexusService="false"

URLPreDomainApt="aptmirror03.sps"
foundAptMirrorService="false"

UrlPreDomain=""

for service in ${services}; do
echo "Getting VPCE IP and URL for ${service}"

    eniId=""
    ip=""
    stDNSSuffix="skytouch.io"

eniId=$(aws ec2 describe-vpc-endpoints  --filters "Name=tag-key,Values=Name" "Name=tag-value,Values=${envLower}-${service}" --query "VpcEndpoints[*].NetworkInterfaceIds[0]" --output text --profile $envLower)

if [[ ${eniId} != "" ]]; then
    ip=$(aws ec2 describe-network-interfaces --network-interface-ids ${eniId} --query "NetworkInterfaces[*].PrivateIpAddress" --output text --profile $envLower)
fi

if [[ ${ip} != "" ]];
then
    echo "found ${service} VPC IP"
    if [[ "${service}" == "bitbucket" ]]; 
    then
        urlPreDomain=${URLPreDomainBB}
        foundBBService="true"
        bbHostEntry="${ip}  ${urlPreDomain}.${stDNSSuffix}"
    elif [[ "${service}" == "nexus" ]]; 
    then
        urlPreDomain=${URLPreDomainNexus}
        foundNexusService="true"
        nexusHostEntry="${ip}  ${urlPreDomain}.${stDNSSuffix}"
    elif [[ "${service}" == "apt-mirror" ]]; 
    then
        urlPreDomain=${URLPreDomainApt}
        foundAptMirrorService="true"
        aptMirrorHostEntry="${ip}  ${urlPreDomain}.${stDNSSuffix}"
    fi
fi
    
    
done

if [[ ${foundBBService} != "true" || ${foundNexusService} != "true" || ${foundAptMirrorService} != "true" ]] ; then
echo "we didnt find one of the service endpoints"
echo "foundBBService:${foundBBService} "
echo "foundNexusService:${foundNexusService} "
echo "foundAptMirrorService:${foundAptMirrorService} "
exit 1;
else
echo "VPC endpoints IPs:"
echo ${bbHostEntry}
echo ${nexusHostEntry}
echo ${aptMirrorHostEntry}
fi