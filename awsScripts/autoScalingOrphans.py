import boto3
import time


session=boto3.Session(profile_name="int")
cfnResource = session.resource('cloudformation')
cfnClient = session.client('cloudformation')
asgClient = session.client('autoscaling')

nonEC2asgStacks = []
   
for stack in cfnResource.stacks.all():
    if "-EC2" in stack.name:
        continue
    print("Getting Resources for "+ stack.name + " to See if ASGs were made by it")
    time.sleep(10)
    for resource in stack.resource_summaries.all():
        if resource.resource_type == "AWS::AutoScaling::AutoScalingGroup":
            print(stack.name +" Has an ASG")
            nonEC2asgStacks.append(stack.name)

print(nonEC2asgStacks)

asgs = asgClient.describe_auto_scaling_groups()['AutoScalingGroups']

asgNoStack = []


for asg in asgs:
    asgTags = asg['Tags']
    stackName = ""
    for tag in asgTags:
        if tag["Key"] == "aws:cloudformation:stack-name":
            stackName = tag["Value"]
            stackExist = True
            try:
                data = cfnClient.describe_stacks(StackName = stackName)
                stackExist = data['Stacks'][0]['StackStatus'] in ["CREATE_COMPLETE","UPDATE_COMPLETE"]
            except ClientError:
                print("Error Getting "+stackName)
                stackExist = False
    if stackName == "" or not stackExist:
        asgNoStack.append(asg["AutoScalingGroupName"])

print(asgNoStack)