$IPArray = @()

$IP = (Get-NetIPAddress -AddressFamily IPv4 | Where-Object -FilterScript { $_.InterfaceAlias -like "Ethernet*" }).IPAddress
$IPArray = $IP.split(".")
$ReverseZoneIp = $IPArray[0]+"."+$IPArray[1]+"."+$IPArray[2]
 
Try 
{

Add-DnsServerPrimaryZone -NetworkID "$ReverseZoneIp.0/24" -ReplicationScope "Domain" -ErrorAction Stop

Write-Output "Reverse zone  created"
}

Catch [Microsoft.Management.Infrastructure.CimException]
{
    Write-Warning "Reverse zone already exists"

}
Finally
{
    $ZoneName = $IPArray[2]+"."+$IPArray[1]+"."+$IPArray[0]+".in-addr.arpa"
    $j = $env:COMPUTERNAME.LastIndexOf("-")
    $j++
    $DomainPrefix =  $env:COMPUTERNAME.Substring($j)
    $DNSName = "$env:COMPUTERNAME.$DomainPrefix.$env:USERDNSDOMAIN".ToLower()
    Try 
    {
        Add-DnsServerResourceRecordPtr -Name $IPArray[3] -ZoneName "$ZoneName" -AllowUpdateAny -TimeToLive 01:00:00 -AgeRecord -PtrDomainName "$DNSName"
        Write-Output "PTR Record created"
    }
    Catch [Microsoft.Management.Infrastructure.CimException]
    {
        Write-Warning "PTR already exists"
    }
} 
