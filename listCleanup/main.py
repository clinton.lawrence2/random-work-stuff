previous_key = None

with open('stackList.txt', 'r') as f, open('newList.txt', 'w') as out:
    for line in f:
        if '-INFORMIX' not in line:
            key = line.split('-')[0]  # Extract the part before the first hyphen
            if key != previous_key:
                if previous_key is not None:
                    out.write('\n')  # Add a new line if the key changes
                previous_key = key
            out.write(line)