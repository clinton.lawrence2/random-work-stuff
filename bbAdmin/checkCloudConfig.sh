#!/bin/zsh

bail_on_error(){
# args 1=return code, 2=success_message, 3=fail_message
    if [ $1 != 0 ]; then
            printf "\nNon zero code return, something failed ${1} ${2} ${3} ${4}\n"
			if [[ $2 == "ymllint" ]]; 
			then
			 	printf "error found writing to file"
				printf "\n~~~~~~~~~~~~~~~Start error writing for Branch $3 $4 ~~~~~~~~~~\n" >> $RUNNING_DIR/ymllint-output.yml
				yamllint -d $RUNNING_DIR/yamllintFlags.yml * >> $RUNNING_DIR/ymllint-output.yml
				printf "\n~~~~~~~~~~~~~~~DONE with $3 $4 ~~~~~~~~~~\n" >> $RUNNING_DIR/ymllint-output.yml
			else
				exit 1
			fi
            
    fi
}

TMP_REPO_DIR="/Users/clawrence/repos-tmp"
REPOS="spring-configuration spring-config-prd spring-configuration-chi spring-configuration-chi-prd"
RUNNING_DIR="$(pwd)"

MonthAgoDate="$(gdate +%m/%d/%Y -d "30 days ago")"

rm -rf $TMP_REPO_DIR
mkdir $TMP_REPO_DIR

for repoName in $REPOS
do
	printf "\n~~~~~~~~~~~~~~~Getting Details for $repoName~~~~~~~~~~\n"		
	
	printf "\nRepo,$repoName\n"

	bail_on_error $?

	cd $TMP_REPO_DIR
	
	git clone ssh://git@scm.skytouch.io/msvc/$repoName.git

	cd $TMP_REPO_DIR/$repoName

	printf "\nFetch $repoName\n"	
	git fetch -f --tags
	bail_on_error $?

	printf "\nCheckout $repoName\n"	
	git checkout master
	bail_on_error $?

	printf "\nPull $repoName\n"	
	git pull
	bail_on_error $?


	printf "\nChecking List of Branches...\n"
	rm -rf $RUNNING_DIR/yamllint-output.yml

    for branch in $(git branch -a | sed 's/^\s*//' | sed 's/^remotes\///' | grep -v 'master$');
    do
		if [[ "$(git log $branch --since "1 months ago" | wc -l)" -eq 0 ]]; then
			# printf "\n $branch is too old \n"
			continue
		else 
			branch=$(echo $branch | sed 's/remotes\/origin\///')
			bail_on_error $?
			printf "\n~~~~~~~~~~~~~~~Checking Branch $repoName $branch for errors~~~~~~~~~~\n"
			git switch -f -q $branch
			bail_on_error $?
			git pull -q
			bail_on_error $?
			yamllint -d $RUNNING_DIR/yamllintFlags.yml * 
			bail_on_error $? "ymllint" $repoName $branch
			printf "\n~~~~~~~~~~~~~~~DONE Checking Branch $repoName $branch for errors~~~~~~~~~~\n"
		fi
    done

	printf "\n~~~~~~~~~~~~~~~End of that $repoName on to the next one~~~~~~~~~~\n"
	
done

printf "\n~~~~~~~~~~~~~~~Done Validating Repos~~~~~~~~~~\n"