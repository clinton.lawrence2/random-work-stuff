import requests
from requests.auth import HTTPBasicAuth
import json

# Replace these with your Bitbucket username and app password
username = 'your_bitbucket_username'
app_password = 'your_bitbucket_app_password'
authKey = "Basic"
headers = {'Authorization': authKey}

# Replace with your Bitbucket workspace and repository slug
workspace = 'skytouchtechnology'
repo_slug = 'zoidberg-clinton-container'

# Base URL for the Bitbucket API
base_url = f'https://api.bitbucket.org/2.0/repositories/{workspace}/{repo_slug}/refs/branches'

def get_branches(url):
    branches = []
    while url:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            data = response.json()
            branches.extend(data['values'])
            url = data.get('next')
        else:
            print(f'Failed to fetch branches. Status code: {response.status_code}')
            url = None
    return branches

def delete_branch(branch_name):
    delete_url = f'{base_url}/{branch_name}'
    delete_response = requests.delete(delete_url, headers=headers)
    if delete_response.status_code == 204:
        print(f'Successfully deleted branch: {branch_name}')
    else:
        print(f'Failed to delete branch: {branch_name}. Status code: {delete_response.status_code}')

# Get all branches
branches = get_branches(base_url)
for branch in branches:
    branch_name = branch['name']
    if 'zoidberg-clinton-container' in branch_name:
        delete_branch(branch_name)
