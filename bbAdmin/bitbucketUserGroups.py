import requests
from base64 import b64encode

def getUserList (url:str) -> list:
    lastPage = False
    nextPage = 0
    permValue=""
    keyValue=""
    userList = []
    
    while(lastPage != True):
        nextPageEndpoint="&start="+str(nextPage)
        finalUrl=url+nextPageEndpoint
        response = requests.get(finalUrl, auth=('clawrence', 'Pad9thujZ!'))
        jsonResponse = response.json()
        valueArray = jsonResponse["values"]
        if (len(valueArray) > 0 ):
            for value in valueArray:
                user = {}
                name=value["displayName"]
                email=value["emailAddress"]
                user["email"] = email
                user["name"] = name
                userList.append(user)
                

        lastPage = jsonResponse["isLastPage"]
        if (lastPage != True):
            nextPage = jsonResponse["nextPageStart"]
    
    return userList



def main():
    
   
    userGroup="jenkins-admins"
    baseURL="https://scm.skytouch.io"
    groups = "/rest/api/1.0/admin/groups/more-members?context="+userGroup

    groupsUrls=baseURL+groups

    users = getUserList(groupsUrls)

    print("==========="+userGroup+" Users============================") 
    for user in users:
        print("Name:"+ user["name"] +" Email:" + user["email"])
    print("====================================") 
    

if __name__ == "__main__":
    main()