import requests
from base64 import b64encode



def getDataArra (url,arraToFill,keyToGrab):
    lastPage = False
    nextPage = 0
    while(lastPage != True):
        nextPageEndpoint="start="+str(nextPage)
        finalUrl=url+nextPageEndpoint
        response = requests.get(finalUrl, auth=('clawrence', ''))
        jsonResponse = response.json()
        valueArray = jsonResponse["values"]
        for value in valueArray:
            arraToFill.append(value[keyToGrab])
        lastPage = jsonResponse["isLastPage"]
        if (lastPage != True):
            nextPage = jsonResponse["nextPageStart"]

def getPermData (repo,url,keyToGrab,csvFile):
    lastPage = False
    nextPage = 0
    permValue=""
    keyValue=""
    while(lastPage != True):
        nextPageEndpoint="start="+str(nextPage)
        finalUrl=url+nextPageEndpoint
        response = requests.get(finalUrl, auth=('clawrence', ''))
        jsonResponse = response.json()
        valueArray = jsonResponse["values"]
        if (len(valueArray) > 0 ):
            for value in valueArray:
                permValue=value["permission"]
                keyValue=value[keyToGrab]["name"]
                print ("repo:"+repo+"; permission:"+ permValue + "; keyToGrab:"+keyToGrab+"; keyValue:"+ keyValue)
                csvFile.write(repo+","+permValue+","+keyToGrab+","+keyValue+"\r\n")

        lastPage = jsonResponse["isLastPage"]
        if (lastPage != True):
            nextPage = jsonResponse["nextPageStart"]
    



def main():
    
   
    
    baseURL="https://scm.skytouch.io"
    projectEndPoint = "/rest/api/1.0/projects?name&permission"

    projectRepoMap ={}
    projectKeyArray=[]
    fullProjURL=baseURL+projectEndPoint+"&"
    getDataArra(fullProjURL,projectKeyArray,"key")
    print("DONE GETTING KEYS")

    for key in projectKeyArray:
        if (key != "ZZZAR"):
            repoSlugArray=[]
            repoEndpoint="/rest/api/1.0/projects/"+key+"/repos"
            fullRepoURL=baseURL+repoEndpoint+"?"
            getDataArra(fullRepoURL,repoSlugArray,"slug")
            projectRepoMap[key]=repoSlugArray
    print (projectRepoMap)
    print("DONE GETTING REPOS")

    # for key in projectRepoMap:
    #     f = open(key+"_bitbucketPerm.csv","w+")
    #     f.write("repo,permission,key,value\r\n")

    #     projectGroupPermEndpoint="/rest/api/1.0/projects/"+key+"/permissions/groups"
    #     fullProjectGroupPermURL=baseURL+projectGroupPermEndpoint+"?"
    #     projectUserPermEndpoint="/rest/api/1.0/projects/"+key+"/permissions/users"
    #     fullProjectUserPermURL=baseURL+projectUserPermEndpoint+"?"
        
    #     getPermData("PROJECT_LEVEL",fullProjectGroupPermURL,"group",f)
    #     getPermData("PROJECT_LEVEL",fullProjectUserPermURL,"user",f)
        
    #     for repo in projectRepoMap[key]:
            
    #         repoGroupPermEndpoint="/rest/api/1.0/projects/"+key+"/repos/"+repo+"/permissions/groups"
    #         fullRepoGroupPermURL=baseURL+repoGroupPermEndpoint+"?"
    #         repoUserPermEndpoint="/rest/api/1.0/projects/"+key+"/repos/"+repo+"/permissions/users"
    #         fullRepoUserPermURL=baseURL+repoUserPermEndpoint+"?"

    #         getPermData(repo,fullRepoGroupPermURL,"group",f)
    #         getPermData(repo,fullRepoUserPermURL,"user",f)
    #     f.close()    

    # f.close()
    print("========DONE With INFO==========") 
    

if __name__ == "__main__":
    main()