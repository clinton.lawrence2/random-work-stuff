#!/bin/sh
repos="chi-vars chi-vars-prd st-vars st-vars-prd spring-configuration spring-config-prd spring-configuration-chi spring-configuration-chi-prd"

bail_on_error(){
# args 1=return code, 2=success_message, 3=fail_message
    if [ $1 != 0 ]; then
            printf "\nNon zero code return, something failed ${1} ${2} ${3}\n"
            exit 1
    fi
}
mkdir ~/branchDetails
for repoName in $repos
do
	printf "\n~~~~~~~~~~~~~~~Getting $repoName~~~~~~~~~~\n"
	cd ~/repos/$repoName
	bail_on_error $?

	git checkout master
	bail_on_error $?
	git fetch -a
	bail_on_error $?
	git pull
	bail_on_error $?
	branchCount=$(git branch -a | grep remotes | wc -l)
	printf "Number Of Branches:$branchCount"

	echo '' > ~/branchDetails/$repoName.csv
	
	printf "BranchCount,$branchCount\n" > ~/branchDetails/$repoName.csv
	
	printf "\nWriting Branch info to $repoName.csv....."
	echo "Branch Name,Date Last Commit,Last Comitter Name,Last Committer Email,Last Commit Message" >>  ~/branchDetails/$repoName.csv 

	git for-each-ref --format='"%(refname)","%(committerdate:relative)","%(authorname)","%(authoremail)","%(contents:subject)"' --sort -committerdate refs/remotes/ >> ~/branchDetails/$repoName.csv
	bail_on_error $?
	printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
	
done
