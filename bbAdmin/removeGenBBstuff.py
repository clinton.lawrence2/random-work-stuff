import requests
import argparse
import boto3

def getPrs(projectKey,repoSlug):
  print("Get PRs from :"+projectKey+","+repoSlug)
  prList = {}
  start = 0
  while True:
    getPRs = "rest/api/1.0/projects/"+projectKey+"/repos/"+repoSlug+"/pull-requests?start="+str(start)
    prs = requests.get(baseUrl+"/"+getPRs, auth=(bbUser, bbPass))
    prsJson = prs.json()
    for pr in prsJson["values"]:
      prData = {}
      prData["title"] = pr["title"]
      prData["version"] = pr["version"]
      prList[pr["id"]] = prData
    isLastPage = prsJson["isLastPage"]
    if not isLastPage:
      start = prsJson["nextPageStart"]
    else:
      break
  return prList

def deletePrs(prList,projectKey,repoSlug,appsRemove):
  print("Deleting PRs from: "+projectKey+'/'+repoSlug)
  ## Update this to match your PR titles as needed
  for key, value in prList.items():  
    if "Genesis Pipeline Commit for:" in value["title"] and any(app in value["title"] for app in appsRemove) :
      deletePr = "/rest/api/1.0/projects/"+projectKey+"/repos/"+repoSlug+"/pull-requests/"+str(key)
      prVersionJson = {'version': value["version"]}
      print("Deleting PR#:"+str(key))
      response = requests.delete(baseUrl+"/"+deletePr, auth=(bbUser, bbPass),json=prVersionJson)
      if 204 != response.status_code:
        print("Failed to Delete PR")
        print("CODE:"+str(response.status_code))
        print(response.text)


def deleteBranches(branchList,projectKey,repoSlug,appsRemove):
  print("Deleting Branches from :"+projectKey+"/"+repoSlug)
  for key,value in branchList.items():
      ## Update this to match your branches as needed  
    if "genesis-pipeline" in key and any(app in key for app in appsRemove)  :
      deleteBranch = "/rest/branch-utils/1.0/projects/"+projectKey+"/repos/"+repoSlug+"/branches"
      deleteBranchJson = {"name": value,"dryRun": False}
      print("Deleting BRANCH:"+str(value))
      response = requests.delete(baseUrl+"/"+deleteBranch, auth=(bbUser, bbPass),json=deleteBranchJson)
      if 204 != response.status_code:
        print("Failed to Delete Branch")
        print("CODE:"+str(response.status_code))
        print(response.text)



def getBranches(projectKey,repoSlug):
  print("Get branches from :"+projectKey+","+repoSlug)
  branchList = {}
  start = 0
  while True:
    getBranches = "rest/api/1.0/projects/"+projectKey+"/repos/"+repoSlug+"/branches?start="+str(start)
    branches = requests.get(baseUrl+"/"+getBranches, auth=(bbUser, bbPass))
    branchesJson = branches.json()
    for branch in branchesJson["values"]:
      branchList[branch["displayId"]] = branch["id"]
    isLastPage = branchesJson["isLastPage"]
    if not isLastPage:
      start = branchesJson["nextPageStart"]
    else:
      break
  return branchList

def getRepos(key):
  print("Get Repos from :"+key)
  repoList = []
  start = 0
  while True:
    getRepos = "/rest/api/1.0/projects/"+key+"/repos?start="+str(start)
    repos = requests.get(baseUrl+"/"+getRepos, auth=(bbUser, bbPass))
    reposJson = repos.json()
    if len(reposJson["values"]) > 0:
      for repo in reposJson["values"]:
        repoList.append(repo["slug"])
      isLastPage = reposJson["isLastPage"]
      if not isLastPage:
        start = reposJson["nextPageStart"]
      else:
        break
  return repoList

def deleteRepo(key,reposlug):
  print("Deleting repo:"+ reposlug+ " from it bucket project :"+key)
  
  deleteRepo = "/rest/api/1.0/projects/"+key+"/repos/"+reposlug
  response = requests.delete(baseUrl+"/"+deleteRepo, auth=(bbUser, bbPass))
  if 202 != response.status_code:
    print("Failed to Delete Repo")
    print("CODE:"+str(response.status_code))
    print(response.text)

def deleteProject(key):
  print("Deleting Bitbucket Project with key:"+key)
  deleteProject = "/rest/api/1.0/projects/"+key
  response = requests.delete(baseUrl+"/"+deleteProject, auth=(bbUser, bbPass))
  if 204 != response.status_code:
    print("Failed to Delete Project")
    print("CODE:"+str(response.status_code))
    print(response.text)

def deleteDownStreamStuff(deleteRepo):
  for projectKey,repos in downStreamReposMap.items():
    for dsRepo in repos:
      prList = getPrs(projectKey,dsRepo)
      deletePrs(prList,projectKey,dsRepo,deleteRepo)
      branchList = getBranches(projectKey,dsRepo)
      deleteBranches(branchList,projectKey,dsRepo,deleteRepo)

def deleteS3File(projectKey,repo):
  session=boto3.Session(profile_name="sps")
  s3Client = session.client('s3')
  print("Deleting "+projectKey+'/'+repo+'.yml from st-project-meta bucket')
  s3Client.delete_object(Bucket='st-project-meta', Key=projectKey+'/'+repo+'.yml')

parser = argparse.ArgumentParser()
parser.add_argument('--bbUser',required=True, help='Your BB Username')
parser.add_argument('--bbPass',required=True, help='Your BB Username')
parser.add_argument('--bbRepo', help='Single BB Repo to Delete. This will also delete all PRs,branches,etc open for this repo, if project key also given, will delete S3 and repo itself')
parser.add_argument('--bbProjKey', help='Single BB Project KEY. If NO REPO given, This will delete the project.\n This means it will delete all PRs,branches,s3 meta for all repos inside the project as well as the repos itself')
args = parser.parse_args()


bbUser = args.bbUser
bbPass = args.bbPass
bbSlug = args.bbRepo
projectKey = args.bbProjKey


##main
downStreamReposMap = {
          "DEVOPS" : ['scruffy-api'],
          "DEPLOY" : ["hos"],
          "MSVC":['spring-config-prd','spring-configuration-chi-prd','spring-configuration','spring-configuration-chi'],
          "AV" :['st-vars','st-vars-prd','chi-vars-prd'] ,
          "INFRA" :['buildtemplates'] 
}


baseUrl = "https://scm.skytouch.io"


if projectKey:
  repos = getRepos(projectKey)
  answer = input("You have decided to delete the entire BB project with key:"+ projectKey+ "\n This will delete the following repos before deleting the project:"+str(repos)+"\n Are you sure you want to do this(Y/N):")
  if answer.lower() == 'y' or answer.lower() == 'yes':
    for repo in repos:
      deleteDownStreamStuff(repo)
      deleteRepo(projectKey,repo)
      deleteS3File(projectKey,repo)
    deleteProject(projectKey)

if bbSlug:
 deleteDownStreamStuff(bbSlug)
 if projectKey:
    deleteRepo(projectKey,bbSlug)
    deleteS3File(projectKey,bbSlug)