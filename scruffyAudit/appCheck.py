from sshtunnel import SSHTunnelForwarder
import requests
import json
import pandas as pd

server = None
remote_user = 'clawrence'
remote_password = ''
remote_host = 'chisps-jumphost.chisps.skytouch.io'
remote_port = 22
local_host = 'localhost'
local_port = 8443
chiScruffyUrl = "chisps-scruffy-api.chisps.skytouch.io"
stScruffyUrl = "sps-scruffy-api.sps.skytouch.io"





st_env = []
chi_env = ["qat1","rit","sit"]


all_env = chi_env + st_env

def startSSHTunnel():
    server = SSHTunnelForwarder(
        (remote_host, remote_port),
        ssh_username=remote_user,
        ssh_password=remote_password,
        remote_bind_address=(chiScruffyUrl, 8443),
        local_bind_address=(local_host, local_port)
    )

    server.start()

def stopSSHTunnel():
    if server != None:
        server.stop() 

startSSHTunnel()

def getApps(env: str) -> dict:
    getAppEndpoint = ":8443/api/horus/envAudit"
    print("calling "+stScruffyUrl+getAppEndpoint)
    
    payload = json.dumps({
      "env": env.upper()
    })

    headers = {
      'Content-Type': 'application/json'
    }
    
    r = requests.request("POST", url = "https://"+stScruffyUrl+getAppEndpoint, headers=headers, data=payload, verify=False)
    response = json.loads(r.text)
    apps = {}
    
    #for each key values from the payload
    for appName,data in response["payload"].items():
        apps[appName] = data["ip"]
    return apps

def confirmActuator(appName: str, ip: str, env: str) -> str:
    apiUrl = ""
   
    proxyScruffyUrl = "localhost"
    actuatorEndpoint = ":8443/api/horus/getActuatorConfigForApp"
    
    if(env in chi_env):
        apiUrl = proxyScruffyUrl
    elif (env in st_env):
        apiUrl = stScruffyUrl

    payload = json.dumps({
        "env": env,
        "app": appName,
        "ip": ip
    })

    headers = {
    'Content-Type': 'application/json'
    }

    print("calling https://"+apiUrl+actuatorEndpoint+" with request:"+payload)
    r = requests.post(url = "https://"+apiUrl+actuatorEndpoint, headers=headers, data=payload, verify=False)
    response = json.loads(r.text)
    status = ""
    
    if(response["payload"] == "not deployed"):
        status = "not deployed"
    elif(response["error"] == True):
        status = "error:"+str(response)
    elif("spring.application.name" in response["payload"].keys()):
        status = "OK"
    else:
        status = "unknown: response obj:"+str(response)

    print(appName + " " + status+"\n")
    return status



def checkApps():
    envsStatus = {}
    for(env) in all_env:
        apps = getApps(env)
        #sort apps alphabetically
        apps = dict(sorted(apps.items()))
        statusOfApps = {}
        for(appName,ip) in apps.items():
            appStatus = confirmActuator(appName,ip,env)
            statusOfApps[appName] = appStatus
        envsStatus[env] = statusOfApps
    return envsStatus

if __name__ == "__main__":
    envsStatus = checkApps()
    stopSSHTunnel()
    with pd.ExcelWriter("appCheck.xlsx") as writer:
        for env, appStatuses in envsStatus.items():
            df1 = pd.DataFrame(appStatuses,index=["status"])
            df1.T.to_excel(writer, sheet_name=env)
        
    