-- tell application "System Events"
--   set zoom_windows to (name of windows of application process "zoom.us")
--   log zoom_windows
--   set inMeeting to (zoom_windows contains "Zoom Meeting" or zoom_windows contains "Zoom Meeting  40-minutes") 
-- end tell

tell application "System Events"
    -- Check if the application process is running
    if exists process "zoom.us" then
        set zoom_windows to (name of windows of application process "zoom.us")
        log zoom_windows
        -- Check if any of the windows have the name you are looking for
        set inMeeting to (zoom_windows contains "Zoom Meeting" or zoom_windows contains "Zoom Meeting  40-minutes")
    else
        set inMeeting to false
    end if
end tell

-- Output result
if inMeeting then
    "true"
else
    "false"
end if

