import argparse
import hashlib
import hmac
import logging
import requests
import time
import subprocess
import os
from crontab import CronTab

logger = logging.getLogger(__name__)

scriptPath = os.path.dirname(os.path.realpath(__file__))
logPath = scriptPath+'/light.log'

# when = 'midnight'  # Rotate logs at midnight
# interval = 1  # Rotate every day
# backup_count = 3  # Number of backup log files to keep
# handler = logging.TimedRotatingFileHandler(log_filename, when=when, interval=interval, backupCount=backup_count)


logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(scriptPath+'/light.log')
sh = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
sh.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(sh)

def sign(clientId,secret,timestamp):
    message = '{}{}'.format(clientId, timestamp)
    signature = hmac.new(bytes(secret , 'latin-1'), msg = bytes(message , 'latin-1'), digestmod = hashlib.sha256).hexdigest().upper()
   
    return signature

def signWToken(clientId,secret,token,timestamp):
    message = '{}{}{}'.format(clientId,token,timestamp)
    signature = hmac.new(bytes(secret , 'latin-1'), msg = bytes(message , 'latin-1'), digestmod = hashlib.sha256).hexdigest().upper()
    
    return signature

def getHeaders():
    epoch = int(time.time() * 1000)
    clientId = "1194d5958540300dwugh"
    secret = "dac770f4c9964dd493f419f7ac1f4d91"
    sig = sign(clientId,secret,epoch)
    headers = {'client_id':clientId ,'access_token': secret,'t':str(epoch),"sign": sig,"sign_method":"HMAC-SHA256"}

    response = requests.get(url="https://openapi.tuyaus.com/v1.0/token?grant_type=1",headers=headers) 

    tokenJson = response.json()
    token = tokenJson["result"]["access_token"]

    sigToken = signWToken(clientId,secret,token,epoch)

    headers2 = {'client_id':clientId ,'access_token': token,'t':str(epoch),"sign": sigToken,"sign_method":"HMAC-SHA256","lang":"en","schema":"hockeyalert"}
    return headers2

def getLightState(deviceId):
    try:
        logging.info("Getting Current Status")
        headers = getHeaders()
        statusUrl = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/status"
        statusJson = requests.get(url=statusUrl,headers = headers)
        statusJson = statusJson.json()
        for result in statusJson["result"]:
            if result["code"] == "switch_led":
                isItOn = result["value"] 
                return isItOn
    except Exception as expression:
        raise expression

def changeState(deviceId,state):
    
    commandJson = None
    currentState = getLightState(deviceId)
    logger.info("Current Light State: "+str(currentState))
    
    newState = True if state == "on" else False

    logger.info("New Light State: "+str(newState))
    
    if currentState != newState:
        commandJson = {'commands':[ {'code':'switch_led','value': newState }]}

        headers2 = getHeaders()
        logger.info("device ID:"+deviceId+" setting state to:"+state)
        commandURL = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/commands"
        requests.post(url=commandURL,headers = headers2, json = commandJson)    

def changeColor(deviceId,color):
    
    commandJson = None

    RED_HSV=[0,255,255]
    GREEN_HSV=[120,255,255]

    HSV = []


    if color == "white":
        commandJson = {"commands":[ {"code": "work_mode","value": "white"}]}
    else:
        if color == "red":
            HSV = RED_HSV
        elif color == "green":
            HSV = GREEN_HSV
        else:
            raise Exception("unknown color " + color+ " given")
        
        h = str(HSV[0])
        s = str(HSV[1])
        v = str(HSV[2])
        commandJson = {"commands":[ {"code": "work_mode","value": "colour"},{"code":"colour_data","value": "{\"h\":"+h+",\"s\":"+s+",\"v\":"+v+"}"}]}
    
    logger.info("changing light:" +deviceId+" to color:" +color)
    commandURL = "https://openapi.tuyaus.com/v1.0/devices/"+deviceId+"/commands"
    
    headers2 = getHeaders() 
    commandResponse = requests.post(url=commandURL,headers = headers2, json = commandJson)
    logger.debug(commandResponse.text)




def getDevices(label):

    headers2 = getHeaders()
    uID = "az1543376701508AxNGr"
    getDevices = "https://openapi.tuyaus.com/v1.0/users/"+uID+"/devices"

    response = requests.get(url=getDevices,headers=headers2)

    devices = response.json()["result"]

    for device in devices:
        if label in device["name"]:
            return device["id"]

def changeLight(status,device):
    if status == "busy":
        changeColor(device,"red")
    elif status == "free" : 
        changeColor(device,"green")
    elif status == "off": 
        changeColor(device,"white")
    elif status == "pause": 
        changeColor(device,"white")
        changeState(device, "off")

def getStatus():
    status = "free"
    #run 
    scriptPath = "/Users/clawrence/repos/clintonsstoof/busyLight/inZoomMeeting.applescript"
    inZoomMeeting = subprocess.run(['osascript', scriptPath],capture_output=True,text=True).stdout.replace("\n","")
    logger.info("Zoom Meeting: "+ inZoomMeeting)

    if inZoomMeeting == "":
        raise Exception("Could not determine if in zoom meeting")

    if str(inZoomMeeting) == "true":
        status="busy"
    return status
    


def main():
    status = ""
    parser = argparse.ArgumentParser(
                        prog = 'Am I Busy',
                        description = 'What is my current work status')

    parser.add_argument("--status",default="",required=False) # positional argument
    args = parser.parse_args()
    status = args.status
    
    curStatus = ""
    device = getDevices("Hallway Bedroom")
    changeState(device,"on")
    updateLight = False

    logger.info("Status: "+status)

    if status == "" :
        logger.info("No status given, will get it based on zoom")
        f = open("./meetingStatus.txt", "r")
        curStatus = f.read()
        f.close()
        

        logger.info("Cur Status: "+curStatus)
        status = getStatus()
        logger.info("New Status: "+status)
        if curStatus != status:
            updateLight = True
    #if status is off check if we are within 25 seconds to the top of the hour if we are wait until top of the hour to continue
    elif status == "off":
        #get current minute
        curSeconds = int(time.strftime("%S"))
        if (not curSeconds > 10) and (not curSeconds < 40):
            logger.info("Almost near top of hour. Will wait until top of hour to continue")
            sleeptime = 70 - curSeconds
            time.sleep(sleeptime)
        updateLight = True
    
    elif status == "pause":
        logger.info("Pausing")
        cronObj = CronTab(user=True)
        jobs = cronObj.find_command(command='/Users/clawrence/repos/clintonsstoof/busyLight/execute.sh')
        for job in jobs:
            job.enable(False)
            cronObj.write()
        updateLight = True
    
    elif status == "unpause":
        logger.info("Unpausing")
        cronObj = CronTab(user=True)
        jobs = cronObj.find_command(command='/Users/clawrence/repos/clintonsstoof/busyLight/execute.sh')
        for job in jobs:
            job.enable(True)
            cronObj.write()
        status = getStatus()
        updateLight = True
    else: 
        updateLight = True
  
    if updateLight:
        logger.info("Updating Status to "+status)
        changeLight(status,device)
        f = open("./meetingStatus.txt", "w")
        f.write(status)
        f.close()

if __name__ == "__main__":
    main()