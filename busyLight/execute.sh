cd /Users/clawrence/repos/clintonsstoof/busyLight

source ./.venv/bin/activate

pip install -r req.txt

if [[ -z "$1" ]]
then
  python busyLight.py
else
  python busyLight.py --status $1
fi