node("AWS_2") 
{
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: 'master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'WipeWorkspace']], gitTool: 'Linux Git', submoduleCfg: [], userRemoteConfigs: [[credentialsId: '0d583dac-f2be-425d-a292-868f7c14cacb', url: 'ssh://git@scm.skytouch.io/pms/choiceadvantage.git']]]
    
    Boolean failure = false
    // Map envs = ["intci":"-Dqa-suite-name=testng-Smoke_Tests.xml",
    //             "crt01":"-Dqa-suite-name=testng-Smoke_Tests.xml",
    //             "qat1":"-DcreateRatesWithPMServletCall=true testng-Choice_Smoke_Test.xml"]
    
    Map parallelRun = [:]

    String badMessagePrefix = "<@UJ7RQGJTT> <@UNKQ0BK0V> <@UEZE3DA0N> <@U02A24MDWTZ> <!subteam^S019JTZ9BM0>\n\n"
    String message = ""
    
    envs.each
    { envUnderTest,envParams ->
      Closure testRun = 
      {
        sh script: "mvn -e -f regression-tests/pom.xml -s /opt/jenkins/settings.xml -gs /opt/jenkins/settings.xml -U integration-test -P regressionWithRetries  ${envParams} -DretriesMaxPerTestClass=2 -DthreadCountOverride=4 -Duse-grid=true -DuseDefaultProperty=false -Dbrowser-name=chrome -DatfEnvironment=${envUnderTest} -DloginCredentialOverrideUser=longusernamemanger -DuseLoginCredentialOverride=false | tee ${envUnderTest}-Log.log"
        String buildLog = readFile file: "${envUnderTest}-Log.log"
        if (buildLog.contains("Failures: 0"))
        {
          println("Tests Succeeded")
          message += "${envUnderTest} IS GOOD\n"
        
        }
        else
        {
          println("${envUnderTest} Tests Failed")
          failure = true
          message += "${envUnderTest} IS BAD!!!!!\n"
        }
        
      }
       parallelRun[env] = testRun
     
    }

   
    parallel parallelRun


    String finalMessage = ""
    if(failure)
    {
      currentBuild.result = 'FAILURE'
      message += "\n\n${env.BUILD_URL}"
      finalMessage = badMessagePrefix + message
    }
    else
    {
      finalMessage = message
    }
  
    Map postMessageObj = readJSON text: '{"channel":"","text": "","thread_ts": ""}'

    postMessageObj.channel = "C04RY8Z70MA"
    postMessageObj.text = finalMessage
    postMessageObj.thread_ts = ""
    
    Boolean debug = true
    String messageSlackUrl = "https://slack.com/api/chat.postMessage"
  
    httpRequest(url: messageSlackUrl ,
      httpMode: "POST", customHeaders: [
        [maskValue: true, name: 'Authorization', value: 'Bearer xoxb-433439644195-4219635647318-dyT1wFqKdZVXcajVpzjSGqjB']
      ],
      acceptType: 'APPLICATION_JSON',
      requestBody: postMessageObj.toString(), contentType: 'APPLICATION_JSON',
      quiet: !debug , validResponseCodes: "200",consoleLogResponseBody: debug)
  
    
}