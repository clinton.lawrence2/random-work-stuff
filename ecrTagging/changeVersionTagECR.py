import argparse_prompt
import boto3
from botocore.exceptions import NoCredentialsError, PartialCredentialsError

def main():
    # Set up argument parser
    parser = argparse_prompt.PromptParser(description='Manage ECR image tags.')
    parser.add_argument('--reponame', help='The name of the ECR repository.')
    parser.add_argument('--action', choices=['delete-tag', 'add-tag'], help='The action to perform.')
    parser.add_argument('--current_tag', help='The current tag of the image.')
    parser.add_argument('--new_tag', help='The new tag to add.If deleting a tag just press enter',default="latest")
    
    # Parse arguments
    args = parser.parse_args()
    
    reponame = args.reponame
    action = args.action
    current_tag = args.current_tag
    new_tag = args.new_tag

    # Initialize the ECR client
    ecr_client = boto3.client('ecr')

    try:
        if action == "delete-tag":
            print(f"Deleting Tag {reponame}:{current_tag}")
            ecr_client.batch_delete_image(
                repositoryName=reponame,
                imageIds=[
                    {'imageTag': current_tag}
                ]
            )
        else:  # action is "add-tag"
            print(f"Adding Tag {reponame}:{new_tag}")
            response = ecr_client.batch_get_image(
                repositoryName=reponame,
                imageIds=[
                    {'imageTag': current_tag}
                ],
                acceptedMediaTypes=['application/vnd.docker.distribution.manifest.v2+json']
            )
            manifest = response['images'][0]['imageManifest']

            ecr_client.put_image(
                repositoryName=reponame,
                imageTag=new_tag,
                imageManifest=manifest
            )
    except (NoCredentialsError, PartialCredentialsError):
        print("AWS credentials not found or incomplete.")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    main()
